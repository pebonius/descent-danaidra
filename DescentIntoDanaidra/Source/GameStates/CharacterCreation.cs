﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class CharacterCreation : GameState
    {
        private const short StartingGold = 250;

        private int highlighted;
        private readonly Stack<GameState> gameStates;
        private readonly InputHandler input;
        private readonly ContentManager content;
        private readonly GraphicsDevice graphics;
        private readonly SoundHandler sound;
        private readonly UIStateHandler ui;
        private readonly PlayerClassList classes;
        private readonly List<PlayerClass> options;
        private readonly ListView statListView;

        private readonly FoodTypeList foodTypeList;
        private readonly ArmorTypeList armorTypeList;
        private readonly WeaponTypeList weaponTypeList;
        private readonly PotionTypeList potionTypeList;
        private readonly WeaponModifierList weaponModifierList;
        private readonly ArmorModifierList armorModifiersList;
        private readonly StatusTypeList statusTypeList;
        private readonly ProjectileTypeList projectileTypeList;
        private readonly SkillTypeList skillTypeList;
        private readonly BookTypeList bookTypeList;
        private readonly ScrapTypeList scrapTypeList;

        public CharacterCreation(ContentManager content, GraphicsDevice graphics, InputHandler input, Stack<GameState> gameStates,
            SoundHandler sound, UIStateHandler ui)
        {
            this.content = content;
            this.gameStates = gameStates;
            this.input = input;
            this.sound = sound;
            this.graphics = graphics;
            this.ui = ui;
            statusTypeList = new StatusTypeList(content);
            projectileTypeList = new ProjectileTypeList(content);
            skillTypeList = new SkillTypeList(content, statusTypeList, projectileTypeList);
            statusTypeList = new StatusTypeList(content);
            projectileTypeList = new ProjectileTypeList(content);
            skillTypeList = new SkillTypeList(content, statusTypeList, projectileTypeList);
            scrapTypeList = new ScrapTypeList(content);
            bookTypeList = new BookTypeList(content, statusTypeList, skillTypeList);
            weaponModifierList = new WeaponModifierList(statusTypeList);
            armorModifiersList = new ArmorModifierList(statusTypeList);
            armorTypeList = new ArmorTypeList(content);
            weaponTypeList = new WeaponTypeList(content);
            potionTypeList = new PotionTypeList(content, statusTypeList);
            foodTypeList = new FoodTypeList(content, statusTypeList);

            classes = new PlayerClassList(content, skillTypeList, armorTypeList, foodTypeList,
                potionTypeList, scrapTypeList, weaponTypeList);

            options = classes.All;
            Font = content.Load<SpriteFont>("fonts/font");
            ButtonTexture = content.Load<Texture2D>("ui/button");
            BgImage = content.Load<Texture2D>("ui/splash");
            Background = new Rectangle(0, 0, this.graphics.Viewport.Width, this.graphics.Viewport.Height);
            highlighted = 0;

            Point size = UIStateHandler.WindowSize;
            Point position = new Point((graphics.Viewport.Width - size.X) / 2, (graphics.Viewport.Height - size.Y) / 2);
            Window = new Window(new Rectangle(position, size), content, Font)
            {
                Title = "Select class",
            };
            
            OptionsPosition = new Point(Window.Box.Left, Window.Box.Top);

            int margin = 25;
            AvatarIcon = new Rectangle(OptionsPosition.X + 100 + margin, Window.Box.Top, Game1.TileSize, Game1.TileSize);
            Rectangle descriptionBox = new Rectangle(AvatarIcon.Left, AvatarIcon.Bottom + margin,
                Window.Box.Right - AvatarIcon.Left - margin, 70);
            CharDescription = new TextBox(options[highlighted].Description, descriptionBox, Font);

            statListView = new ListView(new Point(CharDescription.Box.Left, CharDescription.Box.Bottom), GetListElements(), GetListColumnDefinitions(), 4, false);

            Open();
        }

        public SpriteFont Font { get; private set; }
        public Rectangle Background { get; private set; }
        public Window Window { get; private set; }
        public Texture2D BgImage { get; private set; }
        public Texture2D ButtonTexture { get; private set; }
        public TextBox CharDescription { get; private set; }
        public Rectangle AvatarIcon { get; private set; }
        public Point OptionsPosition { get; private set; }
        public PlayerClass SelectedClass { get; private set; }
        public string SelectedName { get; set; }

        private void CompleteSelection()
        {
            SavePackage savePackage = new SavePackage()
            {
                SaveData = new SaveData
                {
                    PlayerLocationId = "danaidra",
                    PlayerXPos = 54,
                    PlayerYPos = 156,
                    Time = 0,
                    Name = SelectedName,
                    ClassName = SelectedClass.Name,
                    TextureFile = SelectedClass.TextureFile,
                    Gold = StartingGold,
                    Experience = 0,
                    Level = 1,
                    SkillPoints = 0,
                    StatPoints = 0,
                    Vitality = SelectedClass.Vitality,
                    Magic = SelectedClass.Magic,
                    Strength = SelectedClass.Strength,
                    Focus = SelectedClass.Focus,
                    VitalityGain = SelectedClass.VitalityGain,
                    MagicGain = SelectedClass.MagicGain,
                    StrengthGain = SelectedClass.StrengthGain,
                    FocusGain = SelectedClass.FocusGain,
                    CurrentHealth = Progression.GetMaxHealth(SelectedClass.Vitality),
                    CurrentMana = Progression.GetMaxMana(SelectedClass.Magic),
                    CurrentFood = Progression.GetMaxFood(),
                    CurrentSleep = Progression.GetMaxSleep(),
                    IsPermadeath = true,
                    KillCount = 0,
                    StepsTaken = 0,
                },
                Equipment = SelectedClass.StartingEquipment,
                Skills = SelectedClass.StartingSkills,
            };

            gameStates.Push(new ActionScreen(content, graphics, input, gameStates, sound, ui, savePackage));
            Close();
        }

        public void NameCharacter()
        {
            ui.OpenInputPrompt("Name your character", ParseName, 15, false, "You must choose a name!");
        }

        public void ParseName(string value)
        {
            SelectedName = value;
            CompleteSelection();
        }

        public override void Open()
        {
        }

        public override void Close()
        {
            End = true;
        }

        public override void Update(GameTime gameTime)
        {
            sound.Update();

            if (!ui.IsOtherInputBlocked())
            {
                UpdateMenu();
            }
            ui.Update(input, null);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            spriteBatch.Draw(BgImage, Background, Color.White);
            Window.Draw(spriteBatch);
            DrawOptions(spriteBatch);
            CharDescription.Draw(spriteBatch);
            if (options[highlighted].Texture != null)
            {
                spriteBatch.Draw(options[highlighted].Texture, AvatarIcon, Color.White);
            }
            statListView.Draw(spriteBatch);
            ui.Draw(spriteBatch, null);
            spriteBatch.End();
        }

        private void DrawOptions(SpriteBatch spriteBatch)
        {
            if (options.Any())
            {
                for (int i = 0; i < options.Count; i++)
                {
                    Color color = i == highlighted ? Colors.FontHoverColor : Colors.FontWhite;
                    Output.DrawText(spriteBatch, Font, options[i].Name, new Point(OptionsPosition.X, OptionsPosition.Y + i * 25), color);
                }
            }
        }

        private void UpdateMenu()
        {
            if (KeyBinds.CancelKeyHit(input))
            {
                Close();
            }
            if (options.Any())
            {
                if (KeyBinds.ConfirmKeyHit(input))
                {
                    SelectedClass = options[highlighted];
                    NameCharacter();
                }
                else if (KeyBinds.UpKeyHit(input))
                {
                    MoveCursorUp();
                    statListView.RefreshList(GetListElements());
                }
                else if (KeyBinds.DownKeyHit(input))
                {
                    MoveCursorDown();
                    statListView.RefreshList(GetListElements());
                }
            }
        }

        private void MoveCursorDown()
        {
            if (highlighted < options.Count - 1)
            {
                highlighted++;
                CharDescription.DisplayText(options[highlighted].Description);
            }
        }

        private void MoveCursorUp()
        {
            if (highlighted > 0)
            {
                highlighted--;
                CharDescription.DisplayText(options[highlighted].Description);
            }
        }

        private ListColumnDefinition[] GetListColumnDefinitions()
        {
            return new ListColumnDefinition[]
            {
                new ListColumnDefinition("Stat", 125),
                new ListColumnDefinition("Starting", 75),
                new ListColumnDefinition("Gain", 75),
            };
        }

        private List<ListElement> GetListElements()
        {
            List<ListElement> listElements = new List<ListElement>();
            
            if (options[highlighted] != null)
            {
                listElements.Add(new ListElement(new string[] { "Vitality:", options[highlighted].Vitality.ToString(), options[highlighted].VitalityGain.ToString() }));
                listElements.Add(new ListElement(new string[] { "Magic:", options[highlighted].Magic.ToString(), options[highlighted].MagicGain.ToString() }));
                listElements.Add(new ListElement(new string[] { "Strength:", options[highlighted].Strength.ToString(), options[highlighted].StrengthGain.ToString() }));
                listElements.Add(new ListElement(new string[] { "Focus:", options[highlighted].Focus.ToString(), options[highlighted].FocusGain.ToString() }));
            }

            return listElements;
        }
    }
}
