﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class MainMenu : GameState
    {
        private const int MenuOptionSpacing = 30;
        private const int WindowWidth = 200;
        private const int WindowHeight = 125;

        private readonly ContentManager content;
        private readonly GraphicsDeviceManager graphics;
        private readonly InputHandler input;
        private readonly SoundHandler sound;
        private readonly Stack<GameState> gameStates;
        private readonly UIStateHandler ui;
        private readonly SaveHandler saveHandler;

        public MainMenu(ContentManager content, GraphicsDeviceManager graphics, InputHandler input, Stack<GameState> gameStates,
            SoundHandler sound, UIStateHandler ui)
        {
            this.content = content;
            this.gameStates = gameStates;
            this.input = input;
            this.sound = sound;
            this.graphics = graphics;
            this.ui = ui;
            saveHandler = new SaveHandler(null);

            Font = content.Load<SpriteFont>("fonts/font");
            BgImage = content.Load<Texture2D>("ui/splash");
            Background = new Rectangle(Point.Zero, Helper.ScreenSize());
            
            Point size = new Point(WindowWidth, WindowHeight);
            Window = new Window(new Rectangle(Helper.CenterOnScreen(size), size), content, Font)
            {
                Title = "Main menu",
            };

            Open();
        }

        public SpriteFont Font { get; private set; }
        public Window Window { get; private set; }
        public Menu Menu { get; private set; }
        public Rectangle Background { get; private set; }
        public Texture2D BgImage { get; private set; }

        public override void Open()
        {
            SoundHandler.PlayMusic(MusicLibrary.MenuTheme, true);
            RefreshMenu();
        }

        public override void Close()
        {
            End = true;
        }

        public override void Update(GameTime gameTime)
        {
            sound.Update();
            ui.Update(input, null);
            if (!ui.IsOtherInputBlocked())
            {
                Menu.Update(input);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            spriteBatch.Draw(BgImage, Background, Color.White);
            Window.Draw(spriteBatch);
            Menu.Draw(spriteBatch);
            ui.Draw(spriteBatch, null);
            spriteBatch.End();
        }

        private void ContinueGame()
        {
            if (saveHandler.SaveFileExists())
            {
                ActionScreen actionScreen = new ActionScreen(content, graphics.GraphicsDevice, input,
                gameStates, sound, ui, null);
                gameStates.Push(actionScreen);
            }
            else
            {
                ui.OpenMessagePrompt("Error", "Save file could not be found.");
            }
        }

        private void StartNewGame()
        {
            if (saveHandler.SaveFileExists())
            {
                ui.OpenYesNoPrompt("Overwrite save?", "Starting a new game will overwrite your current save. Are you sure?",
                    new Action(() => { gameStates.Push(new CharacterCreation(content, graphics.GraphicsDevice, input, gameStates, sound, ui)); }), null);
            }
            else
            {
                gameStates.Push(new CharacterCreation(content, graphics.GraphicsDevice, input, gameStates, sound, ui));
            }
        }

        private void OpenCredits()
        {
            gameStates.Push(new CreditsScreen(content, graphics, input, gameStates, sound));
        }

        private void QuitGame()
        {
            ui.OpenYesNoPrompt("Exit to Windows", "Are you sure you want to quit?", new Action(() => Close()), null);
        }

        private void RefreshMenu()
        {
            List<MenuOption> mainMenuOptions = new List<MenuOption>
            {
                new MenuOption("Continue game", saveHandler.SaveFileExists(), ContinueGame),
                new MenuOption("New game", true, StartNewGame),
                new MenuOption("Credits", true, OpenCredits),
                new MenuOption("Quit", true, QuitGame)
            };
            Menu = new Menu(mainMenuOptions, new Point(Window.Box.Center.X, Window.Box.Top), Font, MenuOptionSpacing, true, 0);
        }
    }
}
