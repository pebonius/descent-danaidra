﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class DeathScreen : GameState
    {
        private readonly ContentManager content;
        private readonly GraphicsDevice graphics;
        private readonly InputHandler input;
        private readonly SoundHandler sound;
        private readonly Stack<GameState> gameStates;
        private readonly Menu menu;

        public DeathScreen(ContentManager content, GraphicsDevice graphics, InputHandler input, Stack<GameState> gameStates, SoundHandler sound, Player player)
        {
            this.content = content;
            this.gameStates = gameStates;
            this.input = input;
            this.sound = sound;
            this.graphics = graphics;
            Font = content.Load<SpriteFont>("fonts/font");
            BgImage = content.Load<Texture2D>("ui/splash");
            Background = new Rectangle(0, 0, graphics.Viewport.Width, graphics.Viewport.Height);

            Point size = new Point(420, 300);
            Point position = new Point((graphics.Viewport.Width - size.X) / 2, (graphics.Viewport.Height - size.Y) / 2);
            Window = new Window(new Rectangle(position, size), content, Font)
            {
                Title = "R.I.P.",
            };

            DeathMessage = new TextBox(CreateDeathMessage(player), Window.Box, Font);

            FitWindowToText();

            menu = new Menu(new List<MenuOption>() { new MenuOption("Back", true, Close) },
                new Point(Helper.CenterHorizontally((int)Font.MeasureString("Back").X, Window.Box), Window.Box.Bottom - 15),
                Font, 0, false);

            Open();
        }

        public SpriteFont Font { get; private set; }
        public Rectangle Background { get; private set; }
        public Window Window { get; private set; }
        public Texture2D BgImage { get; private set; }
        public TextBox DeathMessage { get; private set; }

        public override void Open()
        {
            SoundHandler.PlayMusic(MusicLibrary.GameoverTheme, false);
        }

        public override void Close()
        {
            End = true;
        }

        public override void Update(GameTime gameTime)
        {
            sound.Update();

            if (KeyBinds.ConfirmKeyHit(input) || KeyBinds.CancelKeyHit(input))
            {
                Close();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            spriteBatch.Draw(BgImage, Background, Color.White);
            Window.Draw(spriteBatch);
            DeathMessage.Draw(spriteBatch);
            menu.Draw(spriteBatch);
            spriteBatch.End();
        }

        private string CreateDeathMessage(Player player)
        {
            string message = player.Name + "'s life in the world of Danaidra ended after taking "
                + player.StepsTaken + " steps. br br ";
            message += player.Name + " has killed " + player.KillCount.Current + " monsters, gaining a total of "
                + player.Experience.Current + " experience, and reaching level " + player.Level.Current + ". br br ";

            if (player.Skills.Any())
            {
                message += player.Name + " has learned " + player.Skills.Count() + " skills. " +
                    "Their most learned skill was " + GetTopSkill(player).Name + " at level "
                    + GetTopSkill(player).Level.Current + ".";
            }
            else
            {
                message += player.Name + " has not learned any skills.";
            }

            return message;
        }

        private Skill GetTopSkill(Player player)
        {
            if (player.Skills.Any())
            {
                return player.Skills.OrderByDescending(item => item.Level.Current).First();
            }
            return null;
        }

        private void FitWindowToText()
        {
            Point size = new Point(UIStateHandler.WindowSize.X, DeathMessage.Height() + 100);
            Window.Rectangle = new Rectangle(Helper.CenterOnScreen(size), size);
            DeathMessage.Box = new Rectangle(Window.Box.Left, Window.Box.Top, Window.Box.Width, 50);
        }
    }
}
