﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ActionScreen : GameState
    {
        private readonly ContentManager content;
        private readonly GraphicsDevice graphics;
        private readonly InputHandler input;
        private readonly SoundHandler sound;
        private readonly Stack<GameState> gameStates;
        private readonly UIStateHandler ui;
        private readonly Camera camera;
        private readonly DebugConsole debug;
        private readonly ObjectParser objectParser;
        private readonly SaveHandler saveHandler;
        private readonly ParticleSystem particles;
        private readonly FloatingTextHandler floatingTexts;
        private readonly TimeOfDay timeOfDay;
        private readonly Lighting lighting;
        private readonly ScreenOverlays overlays;

        // TYPE LISTS
        private readonly StatusTypeList statusTypeList;
        private readonly ProjectileTypeList projectileTypeList;
        private readonly SkillTypeList skillTypeList;
        private readonly TileTypeList tileTypeList;
        private readonly MonsterTypeList monsterTypeList;
        private readonly FoodTypeList foodTypeList;
        private readonly Loot randomLootList;
        private readonly ObstacleTypeList obstacleTypeList;
        private readonly ContainerTypeList containerTypeList;
        private readonly NPCTypeList npcTypeList;
        private readonly ArmorTypeList armorTypeList;
        private readonly WeaponTypeList weaponTypeList;
        private readonly PotionTypeList potionTypeList;
        private readonly WeaponModifierList weaponModifierList;
        private readonly ArmorModifierList armorModifiersList;
        private readonly AltarTypeList altarTypeList;
        private readonly BookTypeList bookTypeList;
        private readonly ScrapTypeList scrapTypeList;

        // PLAYER
        private Player player;

        public ActionScreen(ContentManager content, GraphicsDevice graphics, InputHandler input, 
            Stack<GameState> gameStates, SoundHandler sound, UIStateHandler ui, SavePackage savePackage)
        {
            this.content = content;
            this.gameStates = gameStates;
            this.input = input;
            this.sound = sound;
            this.graphics = graphics;
            this.ui = ui;

            statusTypeList = new StatusTypeList(content);
            projectileTypeList = new ProjectileTypeList(content);
            skillTypeList = new SkillTypeList(content, statusTypeList, projectileTypeList);
            scrapTypeList = new ScrapTypeList(content);
            bookTypeList = new BookTypeList(content, statusTypeList, skillTypeList);
            weaponModifierList = new WeaponModifierList(statusTypeList);
            armorModifiersList = new ArmorModifierList(statusTypeList);
            tileTypeList = new TileTypeList(content);
            containerTypeList = new ContainerTypeList(content);
            obstacleTypeList = new ObstacleTypeList(content);
            armorTypeList = new ArmorTypeList(content);
            weaponTypeList = new WeaponTypeList(content);
            potionTypeList = new PotionTypeList(content, statusTypeList);
            foodTypeList = new FoodTypeList(content, statusTypeList);
            monsterTypeList = new MonsterTypeList(content, statusTypeList, foodTypeList);
            npcTypeList = new NPCTypeList(content, foodTypeList, armorTypeList, armorModifiersList, weaponTypeList,
                weaponModifierList, potionTypeList, bookTypeList);
            randomLootList = new Loot(foodTypeList, potionTypeList, armorTypeList, armorModifiersList,
                weaponTypeList, weaponModifierList, bookTypeList, scrapTypeList);
            altarTypeList = new AltarTypeList(content, statusTypeList);

            objectParser = new ObjectParser(content, ui)
            {
                TileTypeList = tileTypeList,
                ObstacleTypeList = obstacleTypeList,
                MonsterTypeList = monsterTypeList,
                ContainerTypeList = containerTypeList,
                AltarTypeList = altarTypeList,
                NpcTypeList = npcTypeList,
                FoodTypeList = foodTypeList,
                ArmorTypeList = armorTypeList,
                WeaponTypeList = weaponTypeList,
                PotionTypeList = potionTypeList,
                WeaponModifierList = weaponModifierList,
                ArmorModifiersList = armorModifiersList,
                BookTypeList = bookTypeList,
                ScrapTypeList = scrapTypeList,
                SkilleTypeList = skillTypeList,
            };

            saveHandler = new SaveHandler(objectParser);
            SavePackage save = savePackage ?? saveHandler.LoadGame();

            particles = new ParticleSystem();
            floatingTexts = new FloatingTextHandler();
            timeOfDay = new TimeOfDay(save.SaveData.Time);
            objectParser.TimeOfDay = timeOfDay;
            lighting = new Lighting(graphics.Viewport);
            overlays = new ScreenOverlays(graphics.Viewport);
            camera = new Camera(null, Point.Zero, graphics.Viewport);
            InitializeUI();
            Initialize(save);

            debug = new DebugConsole(ui, player, objectParser);
            
            Open();
        }

        public void OnPlayerDied(object source, EventArgs args)
        {
            gameStates.Push(new DeathScreen(content, graphics, input, gameStates, sound, player));
            saveHandler.DeleteSaveFiles();
            Close();
        }
        
        public void OnQuitToMenu(object source, EventArgs args)
        {
            saveHandler.SaveGame(player, timeOfDay);
            Close();
        }

        public override void Open()
        {
        }

        public override void Close()
        {
            ui.ClearStates();
            End = true;
        }

        public override void Update(GameTime gameTime)
        {
            sound.Update();
            floatingTexts.Update();

            int toUpdate = 0;
            ui.Update(input, player);

            if (input.KeyHit(Keys.F1))
            {
                debug.OpenDebugConsole();
            }

            if (!ui.IsOtherInputBlocked())
            {
                toUpdate += player.UpdateStates(input, gameTime);
            }

            for (int turns = 0; turns < toUpdate; turns++)
            {
                UpdateByTurn();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (camera != null && camera.Location != null)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, camera.Transform());
                camera.Location.Tilemap.Draw(spriteBatch, player, camera, lighting, ui);
                spriteBatch.End();
            }

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            particles.Draw(spriteBatch);
            player.DrawStates(spriteBatch);
            overlays.Draw(spriteBatch, player, ui, camera);
            ui.Draw(spriteBatch, player);
            floatingTexts.Draw(spriteBatch, ui.Font, camera);
            spriteBatch.End();
        }
        
        private void Initialize(SavePackage save)
        {
            OutdoorLocation startingLocation = OutdoorLocation.LoadLocation(save.SaveData.PlayerLocationId, content, timeOfDay, tileTypeList, obstacleTypeList, objectParser);

            if (startingLocation != null)
            {
                player = new Player(content, startingLocation, new Point(save.SaveData.PlayerXPos, save.SaveData.PlayerYPos), timeOfDay, lighting, camera, ui, save);
                player.PlayerDied += OnPlayerDied;
                HUD.MessageLog.DisplayMessage("You awaken in " + startingLocation.Name + ".");
                SoundHandler.PlaySound(SoundLibrary.SpookyDrone);
                camera.UpdateByTurn(player);
                lighting.Update(player.Location, camera);
                overlays.Update(player);
                player.Location.UpdateFog(camera);
                HUD.UpdateMinimap(player, tileTypeList);
            }
            else
            {
                Close();
                ui.OpenMessagePrompt("Error", "Could not load player location: <" + save.SaveData.PlayerLocationId + "> br br " +
                    "Save file or location data file might have been corrupted.");
            }
        }
        
        private void InitializeUI()
        {
            ui.ResetToHUD();
            if (ui.uiStates.First() is HUD hud)
            {
                hud.QuitToMenu += OnQuitToMenu;
            }
            HUD.MessageLog.ClearMessages();
        }
        
        private void UpdateByTurn()
        {
            player.Update();
            camera.UpdateByTurn(player);
            player.Location.UpdateObjects(player, camera);
            timeOfDay.Update();
            lighting.Update(player.Location, camera);
            overlays.Update(player);
            HUD.UpdateMinimap(player, tileTypeList);
            if (!player.IsDead)
            {
                saveHandler.SaveGame(player, timeOfDay);
            }
        }
    }
}
