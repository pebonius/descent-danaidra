﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ContainerTypeList
    {
        public ContainerTypeList(ContentManager content)
        {
            Chest = new ContainerType("Chest", 5, content.Load<Texture2D>("objects/chest"));
            Crate = new ContainerType("Crate", 2, content.Load<Texture2D>("objects/crate"));
            Barrel = new ContainerType("Barrel", 2, content.Load<Texture2D>("objects/barrel"));

            Dictionary = new Dictionary<string, ContainerType>()
            {
                { Chest.Name.ToLower(), Chest },
                { Crate.Name.ToLower(), Crate },
                { Barrel.Name.ToLower(), Barrel },
            };
        }

        public Dictionary<string, ContainerType> Dictionary { get; }
        public ContainerType Chest { get; }
        public ContainerType Crate { get; }
        public ContainerType Barrel { get; }
    }
}
