﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class AltarTypeList
    {
        StatusTypeList statusTypes;

        public AltarTypeList(ContentManager content, StatusTypeList statusTypes)
        {
            this.statusTypes = statusTypes;

            Ahtara = new AltarType("Ahtara, goddess of fertility and regeneration", content.Load<Texture2D>("objects/ahtaraAltar"), AhtaraBlessing)
            {
                Light = new Light(0.75f, 2.5f, Color.LightBlue),
            };

            Naku = new AltarType("Naku, deity of magic", content.Load<Texture2D>("objects/nakuAltar"), NakuBlessing)
            {
                Light = new Light(0.75f, 2.5f, Color.LightBlue),
            };

            Sogin = new AltarType("Sogin, god of strength", content.Load<Texture2D>("objects/soginAltar"), SoginBlessing)
            {
                Light = new Light(0.75f, 2.5f, Color.LightBlue),
            };

            Dictionary = new Dictionary<string, AltarType>()
            {
                { Ahtara.Name.ToLower(), Ahtara },
                { Naku.Name.ToLower(), Naku },
                { Sogin.Name.ToLower(), Sogin },
            };
        }

        public AltarType Ahtara { get; private set; }
        void AhtaraBlessing(Player player)
        {
            player.RemoveStatus(statusTypes.Poisoning);
            player.AddStatus(new Status(statusTypes.Regeneration, 100));
        }

        public AltarType Naku { get; private set; }
        void NakuBlessing(Player player)
        {
            HUD.MessageLog.DisplayMessage("Nothing happens.");
        }

        public AltarType Sogin { get; private set; }
        void SoginBlessing(Player player)
        {
            HUD.MessageLog.DisplayMessage("Nothing happens.");
        }

        public Dictionary<string, AltarType> Dictionary { get; }
        public AltarType GetRandomType()
        {
            return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
        }
    }
}
