﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    class Stash : IMapObject
    {
        UIStateHandler ui;

        public Stash(Location location, Point position, Texture2D texture, UIStateHandler ui)
        {
            this.ui = ui;
            location.Tilemap.ChangeObjectLocation(this, location, position);
            Texture = texture;
        }

        public Location Location { get; set; }
        public Point? Position { get; set; }
        public Texture2D Texture { get; private set; }
        public bool IsDead { get; set; }

        public bool OnCollision(Player player)
        {
            ui.OpenStash(player, Texture);
            return false;
        }

        public virtual void Update(Player player)
        {
        }
    }
}