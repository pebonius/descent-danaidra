﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra
{
    public class ObstacleType
    {
        public ObstacleType(Texture2D texture, Texture2D snowTexture)
        {
            Texture = texture;
            SnowTexture = snowTexture;
        }

        public Texture2D Texture { get; private set; }
        public Texture2D SnowTexture { get; private set; }
        public Light Light { get; set; }
    }
}
