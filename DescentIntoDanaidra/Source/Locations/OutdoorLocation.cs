﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class OutdoorLocation : Location
    {
        public OutdoorLocation(int width, int height, TimeOfDay timeOfDay, TileTypeList tileTypes)
            : base(width, height, Color.White, tileTypes)
        {
            TimeOfDay = timeOfDay;
        }

        public OutdoorLocation(LocationData locationData, ContentManager content, TimeOfDay timeOfDay,
            TileTypeList tileTypeList, ObstacleTypeList obstacleTypeList, ObjectParser objectParser)
            : this(0, 0, timeOfDay, tileTypeList)
        {
            Name = locationData.Name;
            Id = locationData.Id;
            Music = content.Load<SoundEffect>("music/" + locationData.MusicFile);
            Tilemap.ReadFromFile(content.Load<Texture2D>("locations/" + locationData.MapFile), tileTypeList);
            Tilemap.ReadObjectsFromImage(content.Load<Texture2D>("locations/" + locationData.StaticsFile), content,
                tileTypeList, obstacleTypeList);
            Tilemap.ReadObjectsFromTxt("Content/locations/" + locationData.ObjectsFile, objectParser);
        }

        public TimeOfDay TimeOfDay { get; private set; }
        public override Color AmbientLight { get { return TimeOfDay.AtmosphericLight(); } }
        
        public static OutdoorLocation LoadLocation(string locationId, ContentManager content, TimeOfDay timeOfDay,
            TileTypeList tileTypeList, ObstacleTypeList obstacleTypeList, ObjectParser objectParser)
        {
            LocationData data = LocationData.LoadFromFile(locationId);

            if (data != null)
            {
                return new OutdoorLocation(data, content, timeOfDay, tileTypeList, obstacleTypeList, objectParser);
            }
            return null;
        }
    }
}
