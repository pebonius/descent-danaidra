﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Dungeon
    {
        private readonly List<DungeonLevel> levels;
        private readonly ContentManager content;
        private readonly TileTypeList tileTypes;
        private readonly MonsterTypeList monsterTypes;
        private readonly ObstacleTypeList obstacleTypes;
        private readonly ContainerTypeList containerTypes;
        private readonly UIStateHandler ui;
        private int maxRoomHeight;
        private int minRoomHeight;
        private int maxRoomWidth;
        private int minRoomWidth;
        private int minWidth;
        private int minHeight;
        private int minRooms;
        private int maxRooms;
        private int totalLevels;

        public Dungeon(Location entranceLocation, Point entrancePosition, ContentManager content, TileTypeList tileTypes,
            MonsterTypeList monsterTypes, ObstacleTypeList obstacleTypes, ContainerTypeList containerTypes, UIStateHandler ui)
        {
            this.content = content;
            this.ui = ui;
            this.tileTypes = tileTypes;
            this.monsterTypes = monsterTypes;
            this.obstacleTypes = obstacleTypes;
            this.containerTypes = containerTypes;

            AmbientLight = Color.Black;

            EntranceLocation = entranceLocation;
            EntrancePosition = entrancePosition;

            levels = new List<DungeonLevel>();

            Entrance = new StairsDown(EntranceLocation, EntrancePosition, this, tileTypes);
        }

        public string Name { get; set; }
        public SoundEffect Music { get; set; }
        public StairsDown Entrance { get; private set; }
        public TileType WallType { get; set; }
        public TileType FloorType { get; set; }
        public Location EntranceLocation { get; private set; }
        public Point EntrancePosition { get; private set; }
        public int TotalLevels { get => totalLevels; set => totalLevels = MathHelper.Clamp(value, 1, 999); }
        public int LevelCount { get { return levels.Count; } }
        public List<MonsterType> Monsters { get; set; }
        public int MonstersPerLevel { get; set; }
        public int ItemsPerLevel { get; set; }
        public int ChestsPerLevel { get; set; }
        public Color AmbientLight { get; set; }
        public int Width { get => minWidth; set => minWidth = MathHelper.Clamp(value, 20, 250); }
        public int Height { get => minHeight; set => minHeight = MathHelper.Clamp(value, 20, 250); }
        public int MinRooms { get => minRooms; set => minRooms = MathHelper.Clamp(value, 2, 250); }
        public int MaxRooms { get => maxRooms; set => maxRooms = MathHelper.Clamp(value, 2, 250); }
        public int MinRoomWidth { get => minRoomWidth; set => minRoomWidth = MathHelper.Clamp(value, 3, 10); }
        public int MaxRoomWidth { get => maxRoomWidth; set => maxRoomWidth = MathHelper.Clamp(value, 3, 10); }
        public int MinRoomHeight { get => minRoomHeight; set => minRoomHeight = MathHelper.Clamp(value, 3, 10); }
        public int MaxRoomHeight { get => maxRoomHeight; set => maxRoomHeight = MathHelper.Clamp(value, 3, 10); }
        public bool Cavelike { get; set; }
        public bool ForceRegularRooms { get; set; }

        public void EnterDungeon(Player player)
        {
            HUD.MessageLog.DisplayMessage("You enter " + Name + ".");
            DungeonLevel levelZero = NewLevel(0);
            player.Location.Tilemap.ChangeObjectLocation(player, levelZero, new Point(levelZero.Entrance.X + 1, levelZero.Entrance.Y));
            player.MovePartyToPlayer();
            SoundHandler.PlaySound(SoundLibrary.SpookyDrone);
        }

        public void ExitDungeon(Player player)
        {
            HUD.MessageLog.DisplayMessage("You exit " + Name + ".");
            player.Location.Tilemap.ChangeObjectLocation(player, EntranceLocation, new Point(EntrancePosition.X - 1, EntrancePosition.Y));
            player.MovePartyToPlayer();
            ClearLevels();
        }

        public DungeonLevel GetLevel(int levelIndex)
        {
            return levels[levelIndex];
        }

        public DungeonLevel NewLevel(int levelIndex)
        {
            var newLevel = new DungeonLevel(this, Width, Height, content, levelIndex, tileTypes, monsterTypes,
                obstacleTypes, containerTypes, ui)
            {
                Name = this.Name + " (Level " + (levelIndex + 1) + ")",
                Music = this.Music,
            };
            levels.Add(newLevel);
            return newLevel;
        }

        public bool HasMonsters()
        {
            return Monsters != null && Monsters.Any();
        }

        public MonsterType GetRandomMonsterType()
        {
            return Monsters[Randomiser.RandomNumber(0, Monsters.Count() - 1)];
        }

        public void ClearLevels()
        {
            levels.Clear();
        }
    }
}
