﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;

namespace DescentIntoDanaidra
{
    [Serializable]
    public class DungeonData
    {
        public string Name { get; set; }
        public string MusicFile { get; set; }
        public string WallType { get; set; }
        public string FloorType { get; set; }
        public int TotalLevels { get; set; }
        public List<string> Monsters { get; set; }
        public int MonstersPerLevel { get; set; }
        public int ItemsPerLevel { get; set; }
        public int ChestsPerLevel { get; set; }
        public int RedAmbient { get; set; }
        public int GreenAmbient { get; set; }
        public int BlueAmbient { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int MinRooms { get; set; }
        public int MaxRooms { get; set; }
        public int MinRoomWidth { get; set; }
        public int MaxRoomWidth { get; set; }
        public int MinRoomHeight { get; set; }
        public int MaxRoomHeight { get; set; }
        public bool Cavelike { get; set; }

        public static DungeonData LoadFromFile(string fileName)
        {
            string filePath = "Content/dungeons/" + fileName.ToLower() + ".xml";
            if (File.Exists(filePath))
            {
                XmlSerializer reader = new XmlSerializer(typeof(DungeonData));
                System.IO.StreamReader file = new System.IO.StreamReader(filePath);
                DungeonData loadedData = (DungeonData)reader.Deserialize(file);
                file.Close();
                return loadedData;
            }
            return null;
        }

        public void SaveToFile(string fileName)
        {
            string filePath = "Content/dungeons/" + fileName.ToLower() + ".xml";
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            XmlSerializer writer = new XmlSerializer(typeof(DungeonData));

            System.IO.FileStream file = System.IO.File.Create(filePath);
            writer.Serialize(file, this);
            file.Close();
        }
    }
}
