﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace DescentIntoDanaidra
{
    public class TileTypeList
    {
        public TileTypeList(ContentManager content)
        {
            Floor = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/floor"), null);
            Goo = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/goo"), null);
            Path = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/path"), null);
            SnowPath = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/snowPath"), null);
            Grass = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/grass"), null);
            Snow = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/snow"), null);
            Pavement = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/pavement"), null);
            StairsDown = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/stairsDown"), null);
            Wall = new TileType(TileType.Kind.wall, content.Load<Texture2D>("tiles/wall"), content.Load<Texture2D>("tiles/wallSide"));
            SnowWall = new TileType(TileType.Kind.wall, content.Load<Texture2D>("tiles/snowWall"), content.Load<Texture2D>("tiles/snowWallSide"));
            GooWall = new TileType(TileType.Kind.wall, content.Load<Texture2D>("tiles/gooWall"), content.Load<Texture2D>("tiles/gooWallSide"));
            Sandstone = new TileType(TileType.Kind.wall, content.Load<Texture2D>("tiles/sandstone"), content.Load<Texture2D>("tiles/sandstoneSide"));
            Pit = new TileType(TileType.Kind.pit, content.Load<Texture2D>("tiles/pit"), content.Load<Texture2D>("tiles/pitSide"));
            Water = new TileType(TileType.Kind.pit, content.Load<Texture2D>("tiles/water"), content.Load<Texture2D>("tiles/waterSide"));
            Door = new TileType(TileType.Kind.floor, content.Load<Texture2D>("tiles/door"), null);
        }

        public TileType Floor { get; }
        public TileType Path { get; }
        public TileType SnowPath { get; }
        public TileType Grass { get; }
        public TileType Snow { get; }
        public TileType Pavement { get; }
        public TileType StairsDown { get; }
        public TileType Wall { get; }
        public TileType SnowWall { get; }
        public TileType Sandstone { get; }
        public TileType Pit { get; }
        public TileType Water { get; }
        public TileType Goo { get; }
        public TileType GooWall { get; }
        public TileType Door { get; }
    }
}
