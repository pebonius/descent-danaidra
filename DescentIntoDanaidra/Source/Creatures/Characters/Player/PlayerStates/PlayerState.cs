﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public abstract class PlayerState
    {
        public bool End { get; protected set; }

        public abstract void Close();

        public abstract int Update(InputHandler input, GameTime gameTime, Player player);

        public abstract void Draw(SpriteBatch spriteBatch, Player player, Camera camera);
    }
}
