﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class PlayerClassList
    {
        public PlayerClassList(ContentManager content, SkillTypeList skillTypes, ArmorTypeList armorTypes,
            FoodTypeList foodTypes, PotionTypeList potionTypes, ScrapTypeList scrapTypes, WeaponTypeList weaponTypes)
        {
            Warrior = new PlayerClass("Warrior", "characters/warrior")
            {
                Texture = content.Load<Texture2D>("characters/warrior"),
                Description = "Strong and sturdy, warriors seem purpose-built to soak up " +
                "big amounts of damage, and reciprocate in kind.",
                Vitality = 15,
                Magic = 4,
                Strength = 7,
                Focus = 6,
                VitalityGain = 60,
                MagicGain = 30,
                StrengthGain = 60,
                FocusGain = 50,
                StartingEquipment = new List<Item>()
                {
                    new Armor(armorTypes.Brigandine, null),
                    new Weapon(weaponTypes.Sword, null),
                    new Food(foodTypes.Bread),
                },
                StartingSkills = new List<Skill>()
                {
                    new Skill(skillTypes.RegenerationSpell),
                },
            };

            MagicUser = new PlayerClass("Magic user", "characters/magicUser")
            {
                Texture = content.Load<Texture2D>("characters/magicUser"),
                Description = "Magic users weave elemental spells or summon creatures from " +
                "other dimensions to avoid close combat.",
                Vitality = 10,
                Magic = 15,
                Strength = 4,
                Focus = 10,
                VitalityGain = 40,
                MagicGain = 60,
                StrengthGain = 40,
                FocusGain = 60,
                StartingEquipment = new List<Item>()
                {
                    new Armor(armorTypes.Robe, null),
                    new Weapon(weaponTypes.Club, null),
                    new Potion(potionTypes.ManaBottle),
                    new Food(foodTypes.Bread),
                },
                StartingSkills = new List<Skill>()
                {
                    new Skill(skillTypes.CastFireball),
                },
            };

            All = new List<PlayerClass> { Warrior, MagicUser, };
        }

        public List<PlayerClass> All { get; private set; }
        public PlayerClass Warrior { get; private set; }
        public PlayerClass MagicUser { get; private set; }
    }
}
