﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Player : Creature, IFighter, IVendor, ILightSource
    {
        private readonly Stack<PlayerState> playerStates;
        private readonly NormalState normalState;
        private readonly double turnsFoodDecline;
        private readonly double turnsSleepDecline;
        private readonly TimeOfDay timeOfDay;
        private readonly Lighting lighting;
        private readonly Camera camera;
        private readonly UIStateHandler ui;
        private int _stepsTaken;
        private const int MaxGold = 99999;
        private const int NumberOfQuickUses = 4;
        private const int MaxKillCount = 999999;
        public const int MaxStashItems = 99;

        public Player(ContentManager content, Location location, Point position, TimeOfDay timeOfDay, Lighting lighting, Camera camera,
            UIStateHandler ui, SavePackage save)
            : base(location, position)
        {
            this.timeOfDay = timeOfDay;
            this.lighting = lighting;
            this.camera = camera;
            this.ui = ui;

            StartingPosition = position;
            StartingLocation = location;

            Equipment = save.Equipment ?? new List<Item>();
            Stash = save.Stash ?? new List<Item>();
            Skills = save.Skills ?? new List<Skill>();
            Statuses = new List<Status>(); //TODO: load from savedata
            QuickUses = new IInventorable[NumberOfQuickUses]; //TODO: load from savedata
            Party = new List<NPC>(); //TODO: load from savedata

            LoadStats(content, save.SaveData);

            Primaries = new List<Stat> { Vitality, Magic, Strength, Focus };

            turnsSleepDecline = TimeOfDay.TurnsPerCycle / Sleep.Max;
            turnsFoodDecline = TimeOfDay.TurnsPerCycle * 0.80d / Sleep.Max;

            Light = new Light(0.5f, 1.5f, Color.White);

            playerStates = new Stack<PlayerState>();
            normalState = new NormalState(ui);
        }

        private void LoadStats(ContentManager content, SaveData saveData)
        {
            Name = saveData.Name;
            ClassName = saveData.ClassName;
            Gold = new Stat("Gold", saveData.Gold, MaxGold);
            Texture = content.Load<Texture2D>(saveData.TextureFile);
            TextureFile = saveData.TextureFile;
            Level = new Stat("Level", saveData.Level, Progression.LevelCap);
            Experience = new Stat("Experience", saveData.Experience, (int)Progression.GetMaxExp(saveData.Level));
            SkillPoints = new Stat("Skill points", saveData.SkillPoints, Progression.SkillPointCap);
            StatPoints = new Stat("Stat points", saveData.StatPoints, Progression.StatPointCap);
            Vitality = new Stat("Vitality", saveData.Vitality, Progression.StatCap, saveData.VitalityGain);
            Magic = new Stat("Magic", saveData.Magic, Progression.StatCap, saveData.MagicGain);
            Strength = new Stat("Strength", saveData.Strength, Progression.StatCap, saveData.StrengthGain);
            Focus = new Stat("Focus", saveData.Focus, Progression.StatCap, saveData.FocusGain);
            KillCount = new Stat("Killcount", saveData.KillCount, MaxKillCount);
            IsPermadeathCharacter = saveData.IsPermadeath;
            Health = new Stat("Health", saveData.CurrentHealth, Progression.GetMaxHealth(saveData.Vitality));
            Mana = new Stat("Mana", saveData.CurrentMana, Progression.GetMaxMana(saveData.Magic));
            Food = new Stat("Food", saveData.CurrentFood, Progression.GetMaxFood());
            Sleep = new Stat("Sleep", saveData.CurrentSleep, Progression.GetMaxSleep());
            StepsTaken = saveData.StepsTaken;
        }

        public event EventHandler PlayerDied;
        public string ClassName { get; set; }
        public override string Name { get; set; }
        public Stat Gold { get; private set; }
        public string TextureFile { get; set; }
        public override Texture2D Texture { get; set; }
        public Stat Experience { get; private set; }
        public Stat Level { get; private set; }
        public Stat SkillPoints { get; private set; }
        public Stat StatPoints { get; private set; }
        public List<Stat> Primaries { get; }
        public Stat Vitality { get; private set; }
        public Stat Magic { get; private set; }
        public Stat Strength { get; private set; }
        public Stat Focus { get; private set; }
        public Stat Health { get; private set; }
        public Stat Mana { get; private set; }
        public bool IsPermadeathCharacter { get; private set; }
        public Stat KillCount { get; private set; }
        public override Location Location { get => base.Location; set { base.Location = value; OnChangedLocation(); } }
        public Stat Food { get; private set; }
        public int FoodFactor { get { return Food.Current == Food.Max ? 1 : Food.Max * 2 / (Food.Current + 1); } }
        public Stat Sleep { get; private set; }
        public int SleepFactor { get { return Sleep.Current == Sleep.Max ? 1 : Sleep.Max / (Sleep.Current + 1); } }
        public int BaseDamage { get { return Strength.Current; } }
        public int BaseDefense { get { return 0; } }
        public Armor EquippedArmor { get; private set; }
        public int DefenseBonus { get { return EquippedArmor != null ? EquippedArmor.Defense : 0; } }
        public Weapon EquippedWeapon { get; private set; }
        public int DamageBonus { get { return EquippedWeapon != null ? EquippedWeapon.Damage : 0; } }
        public List<Item> Equipment { get; }
        public List<Item> Stash { get; }
        public int EquipmentSlots { get { return 0; } }
        public int MaxWeight { get { return Strength.Current * 6 + 10; } }
        public int Weight { get { return Equipment.Sum(b => b is Item item ? item.Weight : 0); } }
        public List<Skill> Skills { get; }
        public IInventorable[] QuickUses { get; set; }
        public List<Status> Statuses { get; private set; }
        public Light Light { get; private set; }
        public Point StartingPosition { get; private set; }
        public Location StartingLocation { get; private set; }
        public int ExpGained { get { return 0; } }
        public int StepsTaken { get => _stepsTaken; set => _stepsTaken = MathHelper.Clamp(value, 0, int.MaxValue); }
        public List<NPC> Party { get; private set; }

        public void Update()
        {
            Health.Regen(1 + Vitality.Current / 20, FoodFactor);
            Mana.Regen(1 + Magic.Current / 10, SleepFactor);
            DeclineStat(Food, 1, (int)Math.Ceiling(turnsFoodDecline), Health);
            DeclineStat(Sleep, 1, (int)Math.Ceiling(turnsSleepDecline), Mana);
            UpdateStatuses();
            StepsTaken++;
        }

        public void CheckLevelUp()
        {
            if (Experience.Current >= Experience.Max)
            {
                LevelUp();
            }
        }

        public void LevelUp()
        {
            if (Level.Current < Level.Max)
            {
                if (Level.Current == 1)
                {
                    ui.OpenMessagePrompt("More power!", "You have just attained a new level. " +
                        "Go to Skills menu to progress skills and Character menu to progress stats.");
                }
                Level.Current++;
                SkillPoints.Current += Progression.SkillPointsPerLevel;
                StatPoints.Current += Progression.StatPointsPerLevel;
                Experience.Current = 0;
                int NewMax = Progression.GetMaxExp(Level.Current);
                Experience.Max = NewMax < Progression.ExpCap ? NewMax : Progression.ExpCap;
                FloatingTextHandler.DisplayMapText(new FloatingText("Level up!", Colors.Yellow, Position.GetValueOrDefault()));
                SoundHandler.PlaySound(SoundLibrary.Shimmer);
                HUD.MessageLog.DisplayMessage("You have attained level " + Level.Current + ".", Colors.Yellow);
                IncreaseStats();
            }
        }

        public void IncreaseExp(int amount)
        {
            if (Level.Current < Level.Max)
            {
                Experience.Current += amount;
                CheckLevelUp();
            }
        }

        public bool CanTakeItem(Item item)
        {
            return Weight + item.Weight <= MaxWeight;
        }

        public bool IsSleepy()
        {
            return Sleep.Max - Sleep.Current > 10;
        }

        public void CheckIfWantToSleep()
        {
            ui.OpenYesNoPrompt("Confirm", "Do you want to go to sleep?", CheckIfSleepy, null);
        }

        public void GoToSleep()
        {
            while (Sleep.Current < Sleep.Max && !IsStarving())
            {
                Sleep.Regen(1, 3);
                timeOfDay.Update();
                Update();
                Location.UpdateSpawns(this);

                if (Sleep.Current == Sleep.Max)
                {
                    ui.OpenMessagePrompt("Hello sleepyhead!", "You sleep well and awake fully rested.");
                }
                else if (IsStarving())
                {
                    ui.OpenMessagePrompt("So hungry...", "The growling of your stomach wakes you up. You are too hungry to be able to sleep.");
                }
            }

            lighting.Update(Location, camera);
        }

        public void ProgressSkill(Skill skill)
        {
            if (SkillPoints.Current > 0)
            {
                if (skill.Level.Current < skill.Level.Max)
                {
                    skill.Level.Current++;
                    SkillPoints.Current--;
                    SoundHandler.PlaySound(SoundLibrary.Shimmer);
                    HUD.MessageLog.DisplayMessage("Your " + skill.Name + " skill has increased.", Colors.Yellow);
                }
                else
                {
                    HUD.MessageLog.DisplayMessage("This skill is at its maximum level.", Colors.Orange);
                }
            }
            else
            {
                HUD.MessageLog.DisplayMessage("You don't have enough skill points.", Colors.Orange);
            }
        }

        public void ProgressStat(Stat stat)
        {
            if (StatPoints.Current > 0)
            {
                if (stat.Current < stat.Max)
                {
                    stat.Current++;
                    StatPoints.Current--;
                }
                else
                {
                    HUD.MessageLog.DisplayMessage("This stat is at its maximum level.", Colors.Orange);
                }
            }
            else
            {
                HUD.MessageLog.DisplayMessage("You don't have enough stat points.", Colors.Orange);
            }
            ResetSecondaryStats();
        }

        public void Attack(Monster monster)
        {
            if (EquippedWeapon != null)
            {
                Combat.MeleeHit(this, monster, EquippedWeapon.InflictEffects);
            }
            else
            {
                Combat.MeleeHit(this, monster, null);
            }
        }

        public void Aim(ProjectileType type, int range, int power, int manaCost)
        {
            if (!playerStates.Any())
            {
                playerStates.Push(new AimProjectile(type, range, power, manaCost, this, camera));
            }
            else
            {
                HUD.MessageLog.DisplayMessage("You are doing something else already.", Colors.Orange);
            }
        }

        public int UpdateStates(InputHandler input, GameTime gameTime)
        {
            if (playerStates.Any())
            {
                if (playerStates.Peek().End)
                {
                    playerStates.Pop();
                }
                else
                {
                    return playerStates.Peek().Update(input, gameTime, this);
                }
            }
            return normalState.Update(input, gameTime, this);
        }

        public void DrawStates(SpriteBatch spriteBatch)
        {
            if (playerStates.Any())
            {
                if (!playerStates.Peek().End)
                {
                    playerStates.Peek().Draw(spriteBatch, this, camera);
                }
            }
        }

        public void Die(IFighter attacker)
        {
            IsDead = true;

            OnPlayerDied();
        }

        public bool TryTakeItem(Item item)
        {
            if (CanTakeItem(item))
            {
                TakeItem(item);
                return true;
            }
            else
            {
                HUD.MessageLog.DisplayMessage("You cannot carry this much.", Colors.Orange);
            }
            return false;
        }

        public void RemoveItem(Item item)
        {
            Equipment.Remove(item);
            RemoveFromQuickUses(item);

            if (item == EquippedArmor)
            {
                EquippedArmor = null;
            }
            else if (item == EquippedWeapon)
            {
                EquippedWeapon = null;
            }
        }

        public bool TryDropItem(Item item)
        {
            Point? pos = NearestFreeToDrop(2);

            if (pos != null)
            {
                if (EquippedWeapon == item || EquippedArmor == item)
                {
                    HUD.MessageLog.DisplayMessage("You must unequip the item first.", Colors.Orange);
                    return false;
                }
                DropItem(item, pos.GetValueOrDefault());
                return true;
            }
            else
            {
                HUD.MessageLog.DisplayMessage("There is no room to drop this item.", Colors.Orange);
                return false;
            }
        }

        public void MovePartyToPlayer()
        {
            foreach (NPC npc in Party)
            {
                Location.Tilemap.ChangeObjectLocation(npc, Location, NearestFreeToTeleport(3));
            }
        }

        public bool EquipArmor(Armor armor)
        {
            if (armor != null)
            {
                if (armor == EquippedArmor)
                {
                    EquippedArmor = null;
                    HUD.MessageLog.DisplayMessage("You unequip " + armor.Name + ".");
                    SoundHandler.PlaySound(SoundLibrary.BagRustle);
                    return true;
                }
                else
                {
                    EquippedArmor = armor;
                    HUD.MessageLog.DisplayMessage("You equip " + armor.Name + ".");
                    SoundHandler.PlaySound(SoundLibrary.BagRustle);
                    return true;
                }
            }

            HUD.MessageLog.DisplayMessage("You cannot equip this armor.", Colors.Orange);
            return false;
        }

        public void DamageArmor()
        {
            if (EquippedArmor != null)
            {
                if (EquippedArmor.Durability.Current > 1)
                {
                    EquippedArmor.Durability.Current--;
                }
                else
                {
                    HUD.MessageLog.DisplayMessage("Your " + EquippedArmor.Name + " was destroyed.", Colors.Orange);
                    RemoveItem(EquippedArmor);
                }
            }
        }

        public bool EquipWeapon(Weapon weapon)
        {
            if (weapon != null)
            {
                if (weapon == EquippedWeapon)
                {
                    EquippedWeapon = null;
                    HUD.MessageLog.DisplayMessage("You unequip " + weapon.Name + ".");
                    SoundHandler.PlaySound(SoundLibrary.BagRustle);
                    return true;
                }
                else
                {
                    EquippedWeapon = weapon;
                    HUD.MessageLog.DisplayMessage("You equip " + weapon.Name + ".");
                    SoundHandler.PlaySound(SoundLibrary.BagRustle);
                    return true;
                }
            }
            HUD.MessageLog.DisplayMessage("You cannot equip this weapon.", Colors.Orange);
            return false;
        }

        public void DamageWeapon()
        {
            if (EquippedWeapon != null)
            {
                if (EquippedWeapon.Durability.Current > 1)
                {
                    EquippedWeapon.Durability.Current--;
                }
                else
                {
                    HUD.MessageLog.DisplayMessage("Your " + EquippedWeapon.Name + " was destroyed.", Colors.Orange);
                    RemoveItem(EquippedWeapon);
                }
            }
        }

        public void AddQuickUse(int number, IInventorable toAdd)
        {
            if (toAdd is Skill || toAdd is IUsable)
            {
                if (QuickUses.Contains(toAdd))
                {
                    RemoveFromQuickUses(toAdd);
                }
                QuickUses[number - 1] = toAdd;
                HUD.MessageLog.DisplayMessage(toAdd.Name + " bound to key " + number + ".");
            }
        }

        public void UpdateStatuses()
        {
            for (int i = 0; i < Statuses.Count; i++)
            {
                if (Statuses[i].TurnsElapsed >= Statuses[i].Lasts)
                {
                    Statuses.RemoveAt(i);
                    i--;
                }
                else
                {
                    Statuses[i].Update(this);
                }
            }
        }

        public void AddStatus(Status status)
        {
            RemoveStatus(status.Type);
            Statuses.Add(status);
        }

        public void RemoveStatus(StatusType type)
        {
            for (int i = 0; i < Statuses.Count; i++)
            {
                if (Statuses[i].Type == type)
                {
                    Statuses.RemoveAt(i);
                    i--;
                }
            }
        }

        public void AskLearnSkill(SkillType type)
        {
            ui.OpenYesNoPrompt("New skill", "Do you wish to learn " + type.Name + "?", new Action(() => TryLearnSkill(type)), null);
        }

        public void Kill(IFighter target)
        {
            IncreaseExp(target.ExpGained);
            KillCount.Current += 1;
        }

        protected virtual void OnPlayerDied()
        {
            PlayerDied?.Invoke(this, EventArgs.Empty);
        }

        protected virtual void OnChangedLocation()
        {
            SoundHandler.PlayMusic(Location.Music, true);
        }

        private void CheckIfSleepy()
        {
            if (IsSleepy() && !IsStarving())
            {
                GoToSleep();
            }
            else if (IsStarving())
            {
                HUD.MessageLog.DisplayMessage("You are too hungry to be able to sleep.", Colors.Orange);
            }
            else
            {
                HUD.MessageLog.DisplayMessage("You don't feel sleepy yet.", Colors.Orange);
            }
        }

        private bool IsStarving()
        {
            return Food.Current < 5;
        }

        private void TakeItem(Item item)
        {
            Equipment.Add(item);
            HUD.MessageLog.DisplayMessage("You take " + item.Name + ".");
            Location.Tilemap.SetItem(null, Position.GetValueOrDefault());
            SoundHandler.PlaySound(SoundLibrary.BagRustle);
        }

        private void DropItem(Item item, Point pos)
        {
            item.Drop(Location, pos);
            RemoveItem(item);
            SoundHandler.PlaySound(SoundLibrary.BagRustle);
            HUD.MessageLog.DisplayMessage("You drop " + item.Name + ".");
        }

        private void IncreaseStats()
        {
            foreach (Stat stat in Primaries)
            {
                if (stat.Current < stat.Max && Randomiser.PassPercentileRoll(stat.Gain))
                {
                    stat.Current++;
                    HUD.MessageLog.DisplayMessage("Your " + stat.Name + " has increased.", Colors.Yellow);
                }
            }
            ResetSecondaryStats();
        }

        private void ResetSecondaryStats()
        {
            Health.Max = Vitality.Current * 5;
            Mana.Max = Magic.Current * 5;
            Health.Current = Health.Max;
            Mana.Current = Mana.Max;
        }

        private void RemoveFromQuickUses(IInventorable toRemove)
        {
            for (int i = 0; i < QuickUses.Length; i++)
            {
                if (QuickUses[i] == toRemove)
                {
                    QuickUses[i] = null;
                    i--;
                }
            }
        }

        private bool TryLearnSkill(SkillType type)
        {
            if (HasSkill(type))
            {
                if (SkillPoints.Current > 0)
                {
                    SkillPoints.Current--;
                    Skills.Add(new Skill(type));
                    HUD.MessageLog.DisplayMessage("You learn " + type.Name + " skill.", Colors.Yellow);
                    SoundHandler.PlaySound(SoundLibrary.Shimmer);
                    return true;
                }
                HUD.MessageLog.DisplayMessage("You do not have enough skill points.", Colors.Orange);
                return false;
            }
            HUD.MessageLog.DisplayMessage("You already know " + type.Name + " skill.", Colors.Orange);
            return false;
        }

        private bool HasSkill(SkillType type)
        {
            return !Skills.Where(s => s is Skill skill && skill.Type == type).Any();
        }
    }
}


