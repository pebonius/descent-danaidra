﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class NPCType
    {
        public NPCType(string title, Texture2D texture)
        {
            Title = title;
            Texture = texture;
            SoldItems = new List<Item>();
        }

        public string Title { get; private set; }
        public Texture2D Texture { get; private set; }
        public List<string> OpeningLines { get; set; }
        public List<string> DialogueLines { get; set; }
        public List<string> GoodbyeLines { get; set; }
        public List<Item> SoldItems { get; set; }
        public CharacterAI AI { get; set; }
        public int Vitality { get; set; }
        public int Magic { get; set; }
        public int Strength { get; set; }
        public int Focus { get; set; }
    }
}
