﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class NPC : Creature, IVendor, ILightSource, IFighter
    {
        private const int goldReplenish = 1200;
        public readonly TimeOfDay timeOfDay;
        public readonly UIStateHandler ui;
        private int _turnsAwayFromAnchor;

        public NPC(Location location, Point position, NPCType type, string name, TimeOfDay timeOfDay, int walkAround, UIStateHandler ui, ContentManager content)
            : base(location, position)
        {
            this.timeOfDay = timeOfDay;
            this.ui = ui;
            Type = type;
            Anchor = position;
            WalkAround = walkAround;
            GivenName = name;
            Equipment = new List<Item>();
            EquipmentSlots = 10;
            Gold = new Stat("Gold", goldReplenish, 99999);
            timeOfDay.NightComes += OnMidnightComes;
            Restock();

            Statuses = new List<Status>();

            Level = new Stat("Level", 1, Progression.LevelCap);
            Experience = new Stat("Experience", 0, (int)Progression.GetMaxExp(Level.Current));
            Vitality = new Stat("Vitality", Type.Vitality, 100);
            Magic = new Stat("Magic", Type.Magic, 100);
            Strength = new Stat("Strength", Type.Strength, 100);
            Focus = new Stat("Focus", Type.Focus, 100);
            Health = new Stat("Health", Type.Vitality * 5);
            Mana = new Stat("Magic", Type.Magic * 5);

            Sleep = new Stat("Sleep", 0);
            Food = new Stat("Food", 0);

            Light = new Light(0.5f, 2f, Color.Tan);
        }

        public NPCType Type { get; private set; }
        public string GivenName { get; private set; }
        public override string Name { get { return GetName(); } }
        public override Texture2D Texture { get { return Type.Texture; } }
        public List<Item> Equipment { get; }
        public int EquipmentSlots { get; }
        public Stat Gold { get; private set; }
        public Point Anchor { get; set; }
        public int WalkAround { get; set; }
        public int TurnsAwayFromAnchor { get => _turnsAwayFromAnchor; set => _turnsAwayFromAnchor = MathHelper.Clamp(value, 0, 999); }
        public Light Light { get; private set; }
        public Weapon EquippedWeapon { get; }
        public Armor EquippedArmor { get; }
        public Stat Vitality { get; private set; }
        public Stat Magic { get; private set; }
        public Stat Strength { get; private set; }
        public Stat Focus { get; private set; }
        public int HealthRegen { get { return 1 + (Vitality.Current + Strength.Current) / 4; } }
        public int ManaRegen { get { return 1 + (Magic.Current + Focus.Current) / 4; } }
        public Stat Health { get; private set; }
        public Stat Mana { get; private set; }
        public int BaseDamage { get { return Strength.Current; } }
        public int BaseDefense { get { return 0; } }
        public int DefenseBonus { get { return 0; } }
        public int DamageBonus { get { return 0; } }
        public Stat Level { get; private set; }
        public Stat Experience { get; private set; }
        public List<Status> Statuses { get; private set; }
        public Stat Sleep { get; private set; }
        public Stat Food { get; private set; }
        public int ExpGained { get { return 0; } }

        public void OnMidnightComes(object source, EventArgs args)
        {
            Restock();
        }

        public bool InventoryFull()
        {
            return Equipment.Count >= EquipmentSlots;
        }

        public override bool OnCollision(Player player)
        {
            if (Type.AI != null)
            {
                return Type.AI.OnCollision(this, player);
            }
            return false;
        }

        public bool HasOpeningLines()
        {
            return Type.OpeningLines != null && Type.OpeningLines.Any();
        }

        public bool HasDialogueLines()
        {
            return Type.DialogueLines != null && Type.DialogueLines.Any();
        }

        public bool HasGoodbyeLines()
        {
            return Type.GoodbyeLines != null && Type.GoodbyeLines.Any();
        }

        public bool SellsItems()
        {
            return Type.SoldItems != null && Type.SoldItems.Any();
        }

        public override void Update(Player player)
        {
            if (Type.AI != null)
            {
                Type.AI.Behave(this, player);
            }
            Health.Regen(HealthRegen, 10);
            Mana.Regen(ManaRegen, 3);
            UpdateStatuses();
        }
        
        public void AddStatus(Status status)
        {
        }

        public void UpdateStatuses()
        {
        }

        public void Die(IFighter attacker)
        {
            IsDead = true;
            HUD.MessageLog.DisplayMessage(Name + " dies.");
            Location.RemoveCreature(this);
        }

        public void IncreaseExp(int amount)
        {
            if (Level.Current < Level.Max)
            {
                Experience.Current += amount;
                CheckLevelUp();
            }
            if (Type.AI is MercenaryAI mercAI && mercAI.FollowedPlayer != null)
            {
                mercAI.FollowedPlayer.IncreaseExp(amount / 2);
            }
        }

        public void Kill(IFighter target)
        {
            IncreaseExp(target.ExpGained);
        }

        public void CheckLevelUp()
        {
            if (Experience.Current >= Experience.Max)
            {
                LevelUp();
            }
        }

        public void LevelUp()
        {
            if (Level.Current < Level.Max)
            {
                Level.Current++;
                Experience.Current = 0;
                int NewMax = Progression.GetMaxExp(Level.Current);
                Experience.Max = NewMax < Progression.ExpCap ? NewMax : Progression.ExpCap;
                FloatingTextHandler.DisplayMapText(new FloatingText("Level up!", Colors.Yellow, Position.GetValueOrDefault()));
                HUD.MessageLog.DisplayMessage(Name + " has attained level " + Level.Current + ".", Colors.Yellow);
                Vitality.Current += 1;
                Magic.Current += 1;
                Strength.Current += 1;
                Focus.Current += 1;
                ResetSecondaryStats();
            }
        }
        
        private string GetName()
        {
            string name = GivenName;
            if (Type.Title != "")
            {
                name += " the " + Type.Title;
            }
            return name;
        }

        private void ResetSecondaryStats()
        {
            Health.Max = Vitality.Current * 5;
            Mana.Max = Magic.Current * 5;
            Health.Current = Health.Max;
            Mana.Current = Mana.Max;
        }

        private void Restock()
        {
            if (Type.SoldItems != null)
            {
                Gold.Current = goldReplenish;
                Equipment.Clear();

                foreach (Item i in Type.SoldItems)
                {
                    if (!InventoryFull())
                    {
                        Equipment.Add(i.Clone());
                    }
                }
            }
        }
    }
}
