﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra
{
    public abstract class Creature : IMoveable
    {
        public Creature(Location location, Point position)
        {
            location.Tilemap.ChangeObjectLocation(this, location, position);
        }

        public virtual Texture2D Texture { get; set; }
        public virtual Location Location { get; set; }
        public virtual string Name { get; set; }
        public Point? Position { get; set; }
        public bool IsDead { get; set; }

        public bool Move(Point targetPosition)
        {
            if (CanMoveToPosition(targetPosition))
            {
                if (CollidingEntity(targetPosition) == null)
                {
                    MoveToPosition(targetPosition);
                    return true;
                }
                else if (CollidingEntity(targetPosition) != null && this is Player player)
                {
                    return CollidingEntity(targetPosition).OnCollision(player);
                }
            }
            return false;
        }

        protected virtual void MoveToPosition(Point targetPosition)
        {
            Location.Tilemap.ChangeObjectPosition(this, targetPosition);
        }

        public bool CanMoveToPosition(Point targetPosition)
        {
            return Location.Tilemap.IsPositionWithinTilemap(targetPosition) && Location.Tilemap.IsWalkable(targetPosition);
        }

        public IMapObject CollidingEntity(Point targetPosition)
        {
            return Location.Tilemap.GetObject(targetPosition);
        }

        public void DeclineStat(Stat declined, int declineAmount, int declineSpeed, Stat penalty)
        {
            if (declined.Current > 0)
            {
                declined.Decline(declineAmount, declineSpeed);
            }
            else if (penalty != null && penalty.Current > 1)
            {
                penalty.Current--;
            }
        }

        bool CanDropItem(Point position)
        {
            return Location.Tilemap.IsWalkable(position) && Location.Tilemap.GetItem(position) == null &&
                (Location.Tilemap.GetObject(position) == null || (Location.Tilemap.GetObject(position) != null &&
                (Location.Tilemap.GetObject(position) is Monster || Location.Tilemap.GetObject(position) is Player ||
                (Location.Tilemap.GetObject(position) is NPC npc) && npc.WalkAround > 0)));
        }

        public Point? NearestFreeToDrop(int maxDistance)
        {
            if (Position != null)
            {
                int distance = 0;

                while (distance < maxDistance)
                {
                    int y = -distance;
                    while (y <= distance)
                    {
                        int x = -distance;

                        while (x <= distance)
                        {
                            Point pos = new Point(Position.GetValueOrDefault().X + x, Position.GetValueOrDefault().Y + y);
                            if (CanDropItem(pos))
                            {
                                return pos;
                            }
                            x++;
                        }
                        y++;
                    }
                    distance++;
                }
            }
            return null;
        }

        public Point? NearestFreeToTeleport(int maxDistance)
        {
            if (Position != null)
            {
                int distance = 0;

                while (distance < maxDistance)
                {
                    int y = -distance;
                    while (y <= distance)
                    {
                        int x = -distance;

                        while (x <= distance)
                        {
                            Point pos = new Point(Position.GetValueOrDefault().X + x, Position.GetValueOrDefault().Y + y);
                            if (CanMoveToPosition(pos) && CollidingEntity(pos) == null)
                            {
                                return pos;
                            }
                            x++;
                        }
                        y++;
                    }
                    distance++;
                }
            }
            return null;
        }

        public virtual bool OnCollision(Player player)
        {
            return false;
        }

        public virtual void Update(Player player)
        {
        }
    }
}
