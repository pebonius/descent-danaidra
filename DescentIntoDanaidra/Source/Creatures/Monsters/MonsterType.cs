﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class MonsterType
    {
        public MonsterType(string name, Texture2D texture)
        {
            Name = name;
            Texture = texture;
        }

        public string Name { get; set; }
        public Texture2D Texture { get; set; }
        public int Vitality { get; set; }
        public int Magic { get; set; }
        public int Strength { get; set; }
        public int Focus { get; set; }
        public int DetectRange { get; set; }
        public int ExpGained { get { return (Vitality + Magic + Strength + Focus + DetectRange) * 2; } }
        public int Difficulty { get { return Vitality + Magic + Strength + Focus + DetectRange; } }
        public Action<IFighter, IFighter> AttackEffect { get; set; }
        public Item[] TypicalDrops { get; set; }
    }
}
