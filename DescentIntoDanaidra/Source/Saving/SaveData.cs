﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;

namespace DescentIntoDanaidra
{
    [Serializable]
    public class SaveData
    {
        public int Time { get; set; }
        public string PlayerLocationId { get; set; }
        public int PlayerXPos { get; set; }
        public int PlayerYPos { get; set; }
        public string Name { get; set; }
        public string TextureFile { get; set; }
        public string ClassName { get; set; }
        public int Gold { get; set; }
        public int Experience { get; set; }
        public int Level { get; set; }
        public int SkillPoints { get; set; }
        public int StatPoints { get; set; }
        public int Vitality { get; set; }
        public int Magic { get; set; }
        public int Strength { get; set; }
        public int Focus { get; set; }
        public int VitalityGain { get; set; }
        public int MagicGain { get; set; }
        public int StrengthGain { get; set; }
        public int FocusGain { get; set; }
        public int CurrentHealth { get; set; }
        public int CurrentMana { get; set; }
        public int CurrentFood { get; set; }
        public int CurrentSleep { get; set; }
        public bool IsPermadeath { get; set; }
        public int KillCount { get; set; }
        public int StepsTaken { get; set; }
        
        public static SaveData CreateSaveData(Player player, TimeOfDay timeOfDay)
        {
            return new SaveData
            {
                Time = timeOfDay.Time,
                PlayerLocationId = player.Location.Id,
                PlayerXPos = player.Position.GetValueOrDefault().X,
                PlayerYPos = player.Position.GetValueOrDefault().Y,
                Name = player.Name,
                TextureFile = player.TextureFile,
                ClassName = player.ClassName,
                Gold = player.Gold.Current,
                Experience = player.Experience.Current,
                Level = player.Level.Current,
                SkillPoints = player.SkillPoints.Current,
                StatPoints = player.StatPoints.Current,
                Vitality = player.Vitality.Current,
                Strength = player.Strength.Current,
                Magic = player.Magic.Current,
                Focus = player.Focus.Current,
                VitalityGain = player.Vitality.Gain,
                MagicGain = player.Magic.Gain,
                StrengthGain = player.Strength.Gain,
                FocusGain = player.Focus.Gain,
                CurrentHealth = player.Health.Current,
                CurrentMana = player.Mana.Current,
                CurrentFood = player.Food.Current,
                CurrentSleep = player.Sleep.Current,
                IsPermadeath = player.IsPermadeathCharacter,
                KillCount = player.KillCount.Current,
                StepsTaken = player.StepsTaken,
            };
        }

        public static SaveData Load(string filePath)
        {
            if (File.Exists(filePath))
            {
                XmlSerializer reader = new XmlSerializer(typeof(SaveData));
                System.IO.StreamReader file = new System.IO.StreamReader(filePath);
                SaveData loadedData = (SaveData)reader.Deserialize(file);
                file.Close();
                return loadedData;
            }
            else
            {
                return null;
            }
        }
    }
}
