﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra.UI
{
    public class ScreenOverlays
    {
        private readonly List<Rectangle> screenBorder;
        const int BorderThickness = 10;
        private Color healthOverlayColor;

        public ScreenOverlays(Viewport viewport)
        {
            screenBorder = new List<Rectangle>() {
            new Rectangle(0, BorderThickness, BorderThickness, viewport.Bounds.Height - BorderThickness * 2),
            new Rectangle(0, 0, viewport.Bounds.Width, BorderThickness),
            new Rectangle(viewport.Bounds.Right - BorderThickness, BorderThickness, BorderThickness, viewport.Bounds.Height - BorderThickness * 2),
            new Rectangle(0, viewport.Bounds.Bottom - BorderThickness, viewport.Bounds.Width, BorderThickness),
            };

            healthOverlayColor = new Color(0, 0, 0);
        }

        public void Update(Player player)
        {
            UpdateHealthOverlay(player);
        }

        private void UpdateHealthOverlay(Player player)
        {
            int diff = (GetHealthDangerPoint(player) - player.Health.Current) * 3;
            healthOverlayColor = new Color(diff + 25, 0, 0, diff);
        }

        public void Draw(SpriteBatch spriteBatch, Player player, UIStateHandler ui, Camera camera)
        {
            DrawLowHealthOverlay(spriteBatch, player, ui, camera);
        }

        private void DrawLowHealthOverlay(SpriteBatch spriteBatch, Player player, UIStateHandler ui, Camera camera)
        { 
            if (player.Health.Current < GetHealthDangerPoint(player))
            {
                DrawScreenBorder(spriteBatch, ui, healthOverlayColor);
            }
        }

        private int GetHealthDangerPoint(Player player)
        {
            return player.Health.Max / 3;
        }

        private void DrawScreenBorder(SpriteBatch spriteBatch, UIStateHandler ui, Color color)
        {
            foreach (Rectangle r in screenBorder)
            {
                spriteBatch.Draw(ui.BarTexture, r, color);
            }
        }
    }
}
