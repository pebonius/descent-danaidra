﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class PropertyDisplay
    {
        private const int IconOffsetY = -3;
        private const int TextOffsetX = 25;

        public PropertyDisplay(Texture2D texture, string label)
        {
            Texture = texture;
            Label = label;
        }

        public Texture2D Texture { get; }
        public string Label { get; }

        public void Draw(SpriteBatch spriteBatch, SpriteFont font, Point position)
        {
            if (Texture != null)
            {
                spriteBatch.Draw(Texture, new Point(position.X, position.Y + IconOffsetY).ToVector2(), Color.White);
            }
            Output.DrawText(spriteBatch, font, Label, new Point(position.X + TextOffsetX, position.Y));
        }
    }
}
