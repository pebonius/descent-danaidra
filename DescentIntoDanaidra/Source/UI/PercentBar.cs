﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class PercentBar
    {
        public PercentBar(Point size, Texture2D texture, Texture2D barTex, Color color, Color bgColor)
        {
            Size = size;
            Texture = texture;
            BarTexture = barTex;
            BarColor = color;
            BgColor = bgColor;
        }

        public Point Size { get; private set; }
        public Texture2D Texture { get; private set; }
        public Texture2D BarTexture { get; private set; }
        public Color BarColor { get; private set; }
        public Color BgColor { get; private set; }

        public void Draw(SpriteBatch spriteBatch, Point position, int Current, int Max)
        {
            Rectangle Box = new Rectangle(position, Size);

            if (Texture != null)
            {
                spriteBatch.Draw(Texture, Box, BgColor);
            }
            
            if(BarTexture != null)
            {
                if (Max != 0)
                {
                    int percentage = 100 * Current / Max;
                    Rectangle bar = new Rectangle(new Point(Box.Left, Box.Top), new Point(Size.X * percentage / 100, Size.Y));
                    spriteBatch.Draw(BarTexture, bar, BarColor);
                }
            }
        }
    }
}
