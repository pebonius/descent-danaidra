﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra.UI
{
    public class StashWindow : UIState
    {
        private const short ListTopOffset = 70;
        private const short DisplayedItems = 6;
        protected const short MaxItemNameLength = 18;
        private readonly UIStateHandler ui;
        protected readonly Point playerIconPosition;
        protected readonly Point stashIconPosition;
        protected readonly Point playerNamePosition;
        protected readonly Point stashNamePosition;
        protected readonly Point playerPropertyPosition;
        protected readonly Point stashPropertyPosition;
        private readonly Texture2D weightIcon;
        private readonly Texture2D bagIcon;
        private readonly Texture2D stashIcon;

        public StashWindow(Player player, Texture2D stashTexture, ContentManager content, UIStateHandler ui)
        {
            this.ui = ui;
            Font = content.Load<SpriteFont>("fonts/font");
            Player = player;
            HighlightedList = 0;

            Point windowSize = UIStateHandler.WindowSize;
            Point windowPosition = new Point((Game1.BufferWidth - windowSize.X) / 2, (Game1.BufferHeight - windowSize.Y) / 2);
            Window = new Window(new Rectangle(windowPosition, windowSize), content, Font)
            {
                Title = "Player stash",
            };

            playerIconPosition = new Point(Window.Box.Left, Window.Box.Top);
            stashIconPosition = new Point(Window.Box.Center.X, Window.Box.Top);
            playerNamePosition = new Point(playerIconPosition.X + 80, playerIconPosition.Y);
            stashNamePosition = new Point(stashIconPosition.X + 80, stashIconPosition.Y);
            playerPropertyPosition = new Point(playerNamePosition.X, playerNamePosition.Y + 20);
            stashPropertyPosition = new Point(stashNamePosition.X, stashNamePosition.Y + 20);
            weightIcon = content.Load<Texture2D>("ui/weight");
            bagIcon = content.Load<Texture2D>("ui/bag");
            stashIcon = stashTexture;

            ResetProperties(player);

            EquipmentListView = new ListView(new Point(Window.Box.Left, Window.Box.Top + ListTopOffset), GetPlayerEquipmentList(player), GetListColumnDefinitions(), DisplayedItems);
            StashListView = new ListView(new Point(Window.Box.Center.X, Window.Box.Top + ListTopOffset), GetPlayerStashList(player), GetListColumnDefinitions(), DisplayedItems, false);

            Open();
        }

        public Window Window { get; }
        public SpriteFont Font { get; }
        protected Player Player { get; }
        protected int HighlightedList { get; set; }
        protected ListView EquipmentListView { get; }
        protected ListView StashListView { get; }
        protected PropertyDisplay PlayerProperty { get; set; }
        protected PropertyDisplay StashProperty { get; set; }

        public override void Update(InputHandler input, Player player)
        {
            if (KeyBinds.CancelKeyHit(input))
            {
                Close();
            }
            else if (KeyBinds.RightKeyHit(input))
            {
                HighlightedList = 1;
                EquipmentListView.IsActive = false;
                StashListView.IsActive = true;
                RefreshListElements(player);
            }
            else if (KeyBinds.LeftKeyHit(input))
            {
                HighlightedList = 0;
                EquipmentListView.IsActive = true;
                StashListView.IsActive = false;
                RefreshListElements(player);
            }
            switch (HighlightedList)
            {
                case 0:
                    UpdatePlayerList(input, player);
                    break;
                case 1:
                    UpdateStashList(input, player);
                    break;
                default:
                    break;
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            Window.Draw(spriteBatch);
            DrawExchangerInfo(spriteBatch, playerIconPosition, Player.Texture, playerNamePosition, Player.Name,
                playerPropertyPosition, PlayerProperty);
            DrawExchangerInfo(spriteBatch, stashIconPosition, stashIcon, stashNamePosition, "Stash",
                stashPropertyPosition, StashProperty);
            EquipmentListView.Draw(spriteBatch);
            StashListView.Draw(spriteBatch);
            DrawInstructions(spriteBatch, new Point(Window.Box.Left, Window.Box.Bottom - 15));
        }

        protected virtual void DrawExchangerInfo(SpriteBatch spriteBatch, Point iconPosition, Texture2D icon,
            Point namePosition, string name, Point propertyPosition, PropertyDisplay property)
        {
            spriteBatch.Draw(icon, iconPosition.ToVector2(), Color.White);
            Output.DrawText(spriteBatch, Font, name, namePosition);
            property.Draw(spriteBatch, Font, propertyPosition);
        }

        private int CalculatePrice(Item item)
        {
            return item.Value * 2;
        }

        protected bool IsSlotEmptyOrNull(List<IInventorable> list, int slot)
        {
            return list.Count <= slot || list[slot] == null;
        }

        protected virtual void UpdatePlayerList(InputHandler input, Player player)
        {
            EquipmentListView.Update(input);

            if (player.Equipment.Count() > EquipmentListView.Cursor && player.Equipment[EquipmentListView.Cursor] is Item item)
            {
                HandlePlayerListInput(input, player, item);
            }
        }

        protected virtual void HandlePlayerListInput(InputHandler input, Player player, Item item)
        {
            if (KeyBinds.ConfirmKeyHit(input))
            {
                TryStoreItem(item, player);
            }
            else if (KeyBinds.DetailsKeyHit(input))
            {
                ui.OpenInventorableDetails(item);
            }
        }

        protected virtual void UpdateStashList(InputHandler input, Player player)
        {
            StashListView.Update(input);

            if (player.Stash.Count() > StashListView.Cursor && player.Stash[StashListView.Cursor] is Item item)
            {
                HandleStashListInput(input, Player, item);
            }
        }

        protected virtual void HandleStashListInput(InputHandler input, Player player, Item item)
        {
            if (KeyBinds.ConfirmKeyHit(input))
            {
                TryTakeItem(item, Player);
            }
            else if (KeyBinds.DetailsKeyHit(input))
            {
                ui.OpenInventorableDetails(item);
            }
        }

        protected virtual void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
            Output.DrawText(spriteBatch, Font, "[Enter] Take/Store [.] Details [-] Close", position);
        }

        private void TryStoreItem(Item item, Player player)
        {
            if (item != player.EquippedArmor && item != player.EquippedWeapon)
            {
                StoreItem(item, player);
                RefreshListElements(player);
            }
            else
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Item equipped!", Colors.Orange, playerIconPosition));
            }
        }

        protected void RefreshListElements(Player player)
        {
            EquipmentListView.RefreshList(GetPlayerEquipmentList(player));
            StashListView.RefreshList(GetPlayerStashList(player));
            ResetProperties(player);
        }

        protected virtual void ResetProperties(Player player)
        {
            PlayerProperty = new PropertyDisplay(weightIcon, "Weight: " + Player.Weight + "/" + Player.MaxWeight);
            StashProperty = new PropertyDisplay(bagIcon, "Items: " + Player.Stash.Count() + "/" + Player.MaxStashItems);
        }

        private void StoreItem(Item item, Player player)
        {
            if (player.Stash.Count() < Player.MaxStashItems)
            {
                player.RemoveItem(item);
                player.Stash.Add(item);
                SoundHandler.PlaySound(SoundLibrary.BagRustle);
            }
            else
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("The stash is full!", Colors.Orange, stashIconPosition));
            }
        }

        private void TryTakeItem(Item item, Player player)
        {
            if (Player.CanTakeItem(item))
            {
                TakeItem(item);
                RefreshListElements(player);
            }
            else
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Overburdened!", Colors.Orange, playerIconPosition));
            }
        }

        private void TakeItem(Item item)
        {
            Player.Equipment.Add(item);
            Player.Stash.Remove(item);
            SoundHandler.PlaySound(SoundLibrary.BagRustle);
        }

        protected virtual ListColumnDefinition[] GetListColumnDefinitions()
        {
            return new ListColumnDefinition[]
            {
                new ListColumnDefinition("Item", 150),
                new ListColumnDefinition("Weight", 75),
            };
        }

        protected virtual List<ListElement> GetPlayerEquipmentList(Player player)
        {
            List<ListElement> listElements = new List<ListElement>();

            if (player.Equipment.Any())
            {
                foreach (Item item in player.Equipment)
                {
                    listElements.Add(new ListElement(new string[] { Helper.TruncateString(item.Name, MaxItemNameLength), item.Weight.ToString() }));
                }
            }
            else
            {
                listElements.Add(new ListElement(new string[] { "empty", }));
            }

            return listElements;
        }

        protected virtual List<ListElement> GetPlayerStashList(Player player)
        {
            List<ListElement> listElements = new List<ListElement>();

            if (player.Stash.Any())
            {
                foreach (Item item in player.Stash)
                {
                    listElements.Add(new ListElement(new string[] { Helper.TruncateString(item.Name, MaxItemNameLength), item.Weight.ToString() }));
                }
            }
            else
            {
                listElements.Add(new ListElement(new string[] { "empty", }));
            }

            return listElements;
        }

        private void Open()
        {
            SoundHandler.PlaySound(SoundLibrary.PageTurn);
        }

        private void Close()
        {
            SoundHandler.PlaySound(SoundLibrary.PageTurnB);
            End = true;
        }
    }
}
