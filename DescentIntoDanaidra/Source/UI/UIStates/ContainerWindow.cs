﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra.UI
{
    public class ContainerWindow : TradeWindow
    {
        private readonly UIStateHandler ui;
        private readonly Texture2D weightIcon;
        private readonly Texture2D bagIcon;

        public ContainerWindow(Player player, IVendor container, ContentManager content, string windowTitle, UIStateHandler ui)
            : base (player, container, content, windowTitle, ui)
        {
            this.ui = ui;
            weightIcon = content.Load<Texture2D>("ui/weight");
            bagIcon = content.Load <Texture2D>("ui/bag");
            ResetProperties();
        }

        protected override void HandlePlayerListInput(InputHandler input, Player player, Item item)
        {
            if (KeyBinds.ConfirmKeyHit(input))
            {
                TryStoreItem(item, player);
            }
            else if (KeyBinds.DetailsKeyHit(input))
            {
                ui.OpenInventorableDetails(item);
            }
        }

        protected override void HandleVendorListInput(InputHandler input, Player player, Item item)
        {
            if (KeyBinds.ConfirmKeyHit(input))
            {
                TryTakeItem(item, player);
            }
            else if (KeyBinds.DetailsKeyHit(input))
            {
                ui.OpenInventorableDetails(item);
            }
        }

        protected override void ResetProperties()
        {
            PlayerProperty = new PropertyDisplay(weightIcon, "Weight: " + Player.Weight + "/" + Player.MaxWeight);
            VendorProperty = new PropertyDisplay(bagIcon, "Items: " + Vendor.Equipment.Count() + "/" + Vendor.EquipmentSlots);
        }

        protected override void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
            Output.DrawText(spriteBatch, Font, "[Enter] Take/Store [.] Details [-] Close", position);
        }

        private void TryTakeItem(Item item, Player player)
        {
            if (Player.CanTakeItem(item))
            {
                Player.Equipment.Add(item);
                SoundHandler.PlaySound(SoundLibrary.BagRustle);
                Vendor.Equipment.Remove(item);
                RefreshListElements(player);
            }
            else
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Overburdened!", Colors.Orange, playerIconPosition));
            }
        }

        private void TryStoreItem(Item item, Player player)
        {
            if (item != Player.EquippedArmor && item != Player.EquippedWeapon &&
                Vendor.Equipment.Count < Vendor.EquipmentSlots)
            {
                Vendor.Equipment.Add(item);
                SoundHandler.PlaySound(SoundLibrary.BagRustle);
                Player.RemoveItem(item);
                RefreshListElements(player);
            }
            else if (Vendor.Equipment.Count >= Vendor.EquipmentSlots)
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Container full!", Colors.Orange, vendorIconPosition));
            }
            else if (item == Player.EquippedArmor || item == Player.EquippedWeapon)
            {
                FloatingTextHandler.DisplayUIText(new FloatingText("Item equipped!", Colors.Orange, playerIconPosition));
            }
        }

        protected override ListColumnDefinition[] GetListColumnDefinitions()
        {
            return new ListColumnDefinition[]
            {
                new ListColumnDefinition("Item", 150),
                new ListColumnDefinition("Weight", 75),
            };
        }

        protected override List<ListElement> GetPlayerListElements(Player player)
        {
            return GetVendorListElements(player);
        }

        protected override List<ListElement> GetVendorListElements(IVendor vendor)
        {
            List<ListElement> listElements = new List<ListElement>();

            if (vendor.Equipment.Any())
            {
                foreach (Item item in vendor.Equipment)
                {
                    listElements.Add(new ListElement(new string[] { Helper.TruncateString(item.Name, MaxItemNameLength), item.Weight.ToString() }));
                }
            }
            else
            {
                listElements.Add(new ListElement(new string[] { "empty", }));
            }

            return listElements;
        }
    }
}
