﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra.UI
{
    public class ReadLines : UIState
    {
        private readonly List<string> linesToRead;
        private int currentLine;
        private readonly Window window;
        private readonly SpriteFont font;
        private readonly TextBox lineTextBox;

        public ReadLines(List<string> lines, string title, ContentManager content)
        {
            linesToRead = lines;
            currentLine = 0;
            font = content.Load<SpriteFont>("fonts/font");
            Point dialogueBoxSize = new Point(Game1.BufferWidth - 100, 100);
            Point dialogueBoxPos = new Point((Game1.BufferWidth - dialogueBoxSize.X) / 2, Game1.BufferHeight - dialogueBoxSize.Y - 25);
            window = new Window(new Rectangle(dialogueBoxPos, dialogueBoxSize), content, font)
            {
                Title = title,
            };
            lineTextBox = new TextBox("", window.Box, font);
            DisplayCurrentLine();
        }

        public override void Update(InputHandler input, Player player)
        {
            if (linesToRead != null)
            {
                HandleKeybinds(input);
            }
            else
            {
                End = true;
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            window.Draw(spriteBatch);
            lineTextBox.Draw(spriteBatch);
        }

        private void HandleKeybinds(InputHandler input)
        {
            if (KeyBinds.ConfirmKeyHit(input))
            {
                TryReadNextLine();
            }
        }

        private void DisplayCurrentLine()
        {
            lineTextBox.DisplayText(linesToRead[currentLine]);
        }

        private void TryReadNextLine()
        {
            if (currentLine < linesToRead.Count - 1)
            {
                currentLine++;
                DisplayCurrentLine();
            }
            else if (currentLine == linesToRead.Count - 1)
            {
                lineTextBox.DisplayText("");
                End = true;
            }
        }
    }
}
