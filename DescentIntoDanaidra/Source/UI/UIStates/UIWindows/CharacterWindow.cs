﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class CharacterWindow : UIWindow
    {
        private const int IconOffsetY = 3;
        private const int Margin = 25;
        private const int PlayerIconOffset = 80;
        private readonly UIStateHandler ui;
        private readonly Texture2D barTexture;
        private readonly Texture2D stasisIcon;
        private readonly Texture2D attackIcon;
        private readonly Texture2D defenseIcon;
        private readonly Texture2D goldIcon;
        private readonly Texture2D weightIcon;
        private readonly Texture2D skillPointIcon;
        private readonly Texture2D statPointIcon;
        private readonly Texture2D stepIcon;
        private readonly Point playerIconPos;
        private readonly Point namePos;
        private readonly Point levelPos;
        private readonly Point expPos;
        private readonly Point propsPos;
        private readonly Point statsPos;
        private readonly Point statusesPos;
        private readonly Point statBarSize;
        private readonly PercentBar exp;
        private readonly List<PercentBar> statPercentBars;
        private readonly ListView listView;
        private List<PropertyDisplay> properties;
        private PropertyDisplay stepsTaken;
        private readonly Point stepsPos;

        public CharacterWindow(ContentManager content, SpriteFont font, UIStateHandler ui, Player player)
            : base(content, font, "Character")
        {
            this.ui = ui;
            ShortcutKey = Keys.C;
            barTexture = ui.BarTexture;
            stasisIcon = content.Load<Texture2D>("statuses/stasis");
            attackIcon = content.Load<Texture2D>("ui/attack");
            defenseIcon = content.Load<Texture2D>("ui/defense");
            goldIcon = content.Load<Texture2D>("ui/gold");
            weightIcon = content.Load<Texture2D>("ui/weight");
            skillPointIcon = content.Load<Texture2D>("ui/skillPoint");
            statPointIcon = content.Load<Texture2D>("ui/statPoint");
            stepIcon = content.Load<Texture2D>("ui/boot");
            statPercentBars = new List<PercentBar>();
            properties = new List<PropertyDisplay>();
            
            exp = new PercentBar(new Point(Window.Box.Width - PlayerIconOffset * 2, 5), barTexture, barTexture, Colors.Orange, Colors.TransparentBarColor);

            namePos = new Point(Window.Box.Left + PlayerIconOffset, Window.Box.Top);
            levelPos = new Point(namePos.X, namePos.Y + Margin);
            expPos = new Point(levelPos.X, levelPos.Y + Margin);
            playerIconPos = new Point(Window.Box.Left, Window.Box.Top);
            statsPos = new Point(Window.Box.Left, Window.Box.Top + PlayerIconOffset);
            statusesPos = new Point(Window.Box.Left + Window.Box.Width / 3, statsPos.Y);
            propsPos = new Point(Window.Box.Left + Window.Box.Width / 3 * 2, statsPos.Y);
            statBarSize = new Point(145, 20);
            stepsPos = new Point(propsPos.X, Window.Box.Top);

            listView = new ListView(new Point(Window.Box.Left, Window.Box.Top + 70), GetListElements(player), GetListColumnDefinitions(), 4);

            Open(player);
        }

        public override void Open(Player player)
        {
            base.Open(player);

            statPercentBars.Clear();
            for (int i = 0; i < player.Primaries.Count; i++)
            {
                Stat s = player.Primaries[i];
                statPercentBars.Add(new PercentBar(statBarSize, barTexture, barTexture, Colors.Green, Colors.TransparentBarColor));
            }

            properties = GetProperties(player);
            stepsTaken = new PropertyDisplay(stepIcon, "Steps: " + player.StepsTaken);
        }

        private List<PropertyDisplay> GetProperties(Player player)
        {
            return new List<PropertyDisplay>()
            {
            new PropertyDisplay(attackIcon, "Damage: " + Combat.GetMinDamage(player) + "-" + Combat.GetMaxDamage(player)),
            new PropertyDisplay(defenseIcon, "Defense: " + Combat.GetMinDefense(player) + "-" + Combat.GetMaxDefense(player)),
            new PropertyDisplay(goldIcon, "Gold: " + player.Gold.Current),
            new PropertyDisplay(weightIcon, "Weight: " + player.Weight + "/" + player.MaxWeight),
            new PropertyDisplay(skillPointIcon, "Skillpoints: " + player.SkillPoints.Current),
            new PropertyDisplay(statPointIcon, "Statpoints: " + player.StatPoints.Current),
            };
        }

        public override void Update(InputHandler input, Player player)
        {
            base.Update(input, player);
            listView.Update(input);

            if (player.Primaries.Count() > listView.Cursor && player.Primaries[listView.Cursor] is Stat stat)
            {
                if (KeyBinds.ProgressKeyHit(input))
                {
                    ui.OpenYesNoPrompt("Confirm", "Are you sure you want to progress " +  stat.Name + "?", new Action(() => ProgressStat(player, stat)), null);
                }
            }
        }

        private void ProgressStat(Player player, Stat stat)
        {
            player.ProgressStat(stat);
            properties = GetProperties(player);
            listView.RefreshList(GetListElements(player));
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            base.Draw(spriteBatch, player);
            spriteBatch.Draw(player.Texture, playerIconPos.ToVector2(), Color.White);
            Output.DrawText(spriteBatch, Font, player.Name + " (" + player.ClassName + ")", namePos);
            Output.DrawText(spriteBatch, Font, "Level " + player.Level.Current, levelPos);
            exp.Draw(spriteBatch, expPos, player.Experience.Current, player.Experience.Max);
            DrawStats(spriteBatch, statsPos, player);
            DrawStatuses(spriteBatch, statusesPos, player);
            DrawProperties(spriteBatch, propsPos);
            listView.Draw(spriteBatch);
            stepsTaken.Draw(spriteBatch, Font, stepsPos);
            DrawInstructions(spriteBatch, new Point(Window.Box.Left, Window.Box.Bottom - 15));
        }

        private void DrawStats(SpriteBatch spriteBatch, Point position, Player player)
        {
            Output.DrawText(spriteBatch, Font, "Stats:", position);
            for (int i = 0; i < statPercentBars.Count; i++)
            {
                Stat s = player.Primaries[i];
                PercentBar p = statPercentBars[i];

                Point barPos = new Point(Window.Box.Left, position.Y + (i + 1) * Margin - IconOffsetY);
                p.Draw(spriteBatch, barPos, s.Current, s.Max);
            }
        }
        
        private void DrawStatuses(SpriteBatch spriteBatch, Point position, Player player)
        {
            Output.DrawText(spriteBatch, Font, "Effects:", position);
            if (player.Statuses.Any())
            {
                for (int i = 0; i < player.Statuses.Count; i++)
                {
                    int offset = player.Statuses.Count < 6 ? Margin : (Window.Box.Bottom - position.Y + Margin) / player.Statuses.Count;
                    Point pos = new Point(position.X, position.Y + offset * i + Margin);
                    spriteBatch.Draw(player.Statuses[i].Icon, new Point(pos.X, pos.Y - IconOffsetY).ToVector2(), Color.White);
                    Point stringPos = new Point(pos.X + Margin, pos.Y);
                    Output.DrawText(spriteBatch, Font, player.Statuses[i].Name, stringPos);
                }
            }
            else
            {
                spriteBatch.Draw(stasisIcon, new Point(position.X, position.Y + Margin - IconOffsetY).ToVector2(), Color.White);
                Output.DrawText(spriteBatch, Font, "Stasis", new Point(position.X + Margin, position.Y + Margin));
            }
        }

        private void DrawProperties(SpriteBatch spriteBatch, Point position)
        {
            Output.DrawText(spriteBatch, Font, "Properties:", position);
            for (int i = 0; i < properties.Count; i++)
            {
                properties[i].Draw(spriteBatch, Font, new Point(position.X, position.Y + i * Margin + Margin));
            }
        }

        private void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
            Output.DrawText(spriteBatch, Font, "[-] Close  [+] Progress", position);
        }

        private ListColumnDefinition[] GetListColumnDefinitions()
        {
            return new ListColumnDefinition[]
            {
                new ListColumnDefinition("", 80),
                new ListColumnDefinition("", 50),
            };
        }

        private List<ListElement> GetListElements(Player player)
        {
            List<ListElement> listElements = new List<ListElement>();

            foreach (Stat stat in player.Primaries)
            {
                listElements.Add(new ListElement(new string[] { stat.Name, stat.Current.ToString() }));
            }

            return listElements;
        }
    }
}
