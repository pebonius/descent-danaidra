﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class UIWindow : UIState
    {
        public UIWindow(ContentManager content, SpriteFont font, string name)
        {
            Name = name;
            Point size = UIStateHandler.WindowSize;
            Point position = new Point((Game1.BufferWidth - size.X) / 2, (Game1.BufferHeight - size.Y) / 2);
            Window = new Window(new Rectangle(position, size), content, font)
            {
                Title = Name,
            };
            Font = font;
        }

        public Window Window { get; private set; }
        public string Name { get; private set; }
        public SpriteFont Font { get; private set; }
        public Keys ShortcutKey { get; set; }

        public virtual void Open(Player player)
        {
            SoundHandler.PlaySound(SoundLibrary.PageTurn);
        }

        public void Close()
        {
            SoundHandler.PlaySound(SoundLibrary.PageTurnB);
            End = true;
        }

        public override void Update(InputHandler input, Player player)
        {
            if (KeyBinds.CancelKeyHit(input) || input.KeyHit(ShortcutKey))
            {
                Close();
            }
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            Window.Draw(spriteBatch);
        }
    }
}
