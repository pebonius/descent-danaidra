﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace DescentIntoDanaidra.UI
{
    public class SkillsWindow : InventoryWindow
    {
        private readonly Texture2D skillPoint;
        private readonly UIStateHandler ui;
        private readonly ListView listView;

        public SkillsWindow(ContentManager content, SpriteFont font, Player player, UIStateHandler ui)
            : base(content, font, "Skills", player)
        {
            this.ui = ui;
            ShortcutKey = Keys.S;
            skillPoint = content.Load<Texture2D>("ui/skillPoint");

            listView = new ListView(new Point(Window.Box.Left + 30, Window.Box.Top), GetListElements(player), GetListColumnDefinitions(), 8);
        }

        public override void Update(InputHandler input, Player player)
        {
            base.Update(input, player);

            listView.Update(input);

            if (player.Skills.Count() > listView.Cursor && player.Skills[listView.Cursor] is Skill skill)
            {
                if (input.KeyHit(Keys.D1))
                {
                    player.AddQuickUse(1, skill);
                }
                else if (input.KeyHit(Keys.D2))
                {
                    player.AddQuickUse(2, skill);
                }
                else if (input.KeyHit(Keys.D3))
                {
                    player.AddQuickUse(3, skill);
                }
                else if (input.KeyHit(Keys.D4))
                {
                    player.AddQuickUse(4, skill);
                }
                else if (KeyBinds.ProgressKeyHit(input))
                {
                    ui.OpenYesNoPrompt("Confirm", "Are you sure you want to progress " + skill.Name + "?", new Action(() => ProgressSkill(player, skill)), null);
                }
                else if (KeyBinds.DetailsKeyHit(input))
                {
                    ui.OpenInventorableDetails(skill);
                }
            }
        }

        private void ProgressSkill(Player player, Skill skill)
        {
            player.ProgressSkill(skill);
            listView.RefreshList(GetListElements(player));
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            base.Draw(spriteBatch, player);

            listView.Draw(spriteBatch);

            int bottomOffset = 50;
            Point pointsPos = new Point(Window.Box.Left, Window.Box.Bottom - bottomOffset);
            spriteBatch.Draw(skillPoint, pointsPos.ToVector2(), Color.White);
            Output.DrawText(spriteBatch, Font, "Skillpoints: " + player.SkillPoints.Current,
                new Point(pointsPos.X + 25, pointsPos.Y + 5));
        }

        protected override void DrawInstructions(SpriteBatch spriteBatch, Point position)
        {
            Output.DrawText(spriteBatch, Font, "[1][2][3][4] Bind  [+] Progress  [.] See details", position);
        }

        private void TryUseSkill(Player player, Skill skill)
        {
            if (skill.CanUse(player))
            {
                skill.Use(player);
                Close();
            }
        }

        private ListColumnDefinition[] GetListColumnDefinitions()
        {
            return new ListColumnDefinition[]
            {
                new ListColumnDefinition("Name", 250),
                new ListColumnDefinition("Level", 75),
                new ListColumnDefinition("Mana", 75),
            };
        }

        private List<ListElement> GetListElements(Player player)
        {
            List<ListElement> listElements = new List<ListElement>();

            foreach (Skill skill in player.Skills)
            {
                listElements.Add(new ListElement(new string[] { skill.Name, skill.Level.Current + "/" + skill.Level.Max, skill.ManaCost.ToString() }));
            }

            return listElements;
        }
    }
}
