﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra.UI
{
    public class DialogueMenu : UIState
    {
        private readonly Menu menu;
        private readonly NPC npc;
        private readonly Player player;
        private readonly TimeOfDay timeOfDay;
        private readonly Window window;
        private readonly SpriteFont font;
        private readonly ContentManager content;
        private readonly UIStateHandler ui;

        public DialogueMenu(NPC npc, Player player, TimeOfDay timeOfDay, ContentManager content, UIStateHandler ui)
        {
            this.content = content;
            this.ui = ui;
            this.npc = npc;
            this.player = player;
            this.timeOfDay = timeOfDay;
            font = content.Load<SpriteFont>("fonts/font");
            Point dialogueBoxSize = new Point(Game1.BufferWidth - 100, 100);
            Point dialogueBoxPos = new Point((Game1.BufferWidth - dialogueBoxSize.X) / 2, Game1.BufferHeight - dialogueBoxSize.Y - 25);
            window = new Window(new Rectangle(dialogueBoxPos, dialogueBoxSize), content, font)
            {
                Title = npc.Name,
            };

            List<MenuOption> options = new List<MenuOption>();
            if (npc.HasDialogueLines())
            {
                options.Add(new MenuOption("Talk", true, Talk));
            }
            if (npc.SellsItems())
            {
                options.Add(new MenuOption("Trade", true, Trade));
            }
            options.Add(new MenuOption("Goodbye", true, Exit));

            menu = new Menu(options, new Point(window.Box.Left, window.Box.Top), font, 25, false);
        }

        void Talk()
        {
            ui.OpenDialogueLines(npc.Type.DialogueLines, npc.Name);
        }

        void Trade()
        {
            if (npc is IVendor vendor && timeOfDay.IsDay())
            {
                ui.OpenTrade(player, vendor);
            }
            else
            {
                ui.OpenDialogueLines(new List<string> { "The store is closed - please come during the day." }, npc.Name);
            }
        }

        void Exit()
        {
            if (npc.HasGoodbyeLines())
            {
                ui.OpenDialogueLines(npc.Type.GoodbyeLines, npc.Name);
            }
            Close();
        }

        public override void Update(InputHandler input, Player player)
        {
            menu.Update(input);
        }

        public override void Draw(SpriteBatch spriteBatch, Player player)
        {
            window.Draw(spriteBatch);
            menu.Draw(spriteBatch);
        }

        private void Close()
        {
            End = true;
        }
    }
}
