﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class Stat
    {
        int regenCounter = 0;
        int declineCounter = 0;
        int current;

        public Stat(string name, int max)
        {
            Name = name;
            Max = MathHelper.Clamp(max, 1, int.MaxValue);
            Current = Max;
        }

        public Stat(string name, int current, int max)
            : this(name, max)
        {
            Current = current;
        }

        public Stat(string name, int current, int max, int gain)
            : this(name, current, max)
        {
            Gain = gain;
        }

        public string Name { get; private set; }
        public int Gain { get; set; }
        public int Max { get; set; }
        public int Current { get { return current; } set { current = MathHelper.Clamp(value, 0, Max); } }
        
        public void Regen(int amount, int delay)
        {
            if (amount > 0 && Current < Max)
            {
                if (regenCounter >= delay)
                {
                    regenCounter = 0;
                    Current += amount;
                }
                else
                {
                    regenCounter++;
                }
            }
        }

        public void Decline(int amount, int delay)
        {
            if (amount > 0 && Current > 0)
            {
                if (declineCounter >= delay)
                {
                    declineCounter = 0;
                    Current -= amount;
                }
                else
                {
                    declineCounter++;
                }
            }
        }
    }
}
