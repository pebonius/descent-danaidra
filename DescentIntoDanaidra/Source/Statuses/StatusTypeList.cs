﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class StatusTypeList
    {
        public StatusTypeList(ContentManager content)
        {
            Regeneration = new StatusType("Regenerating", content.Load<Texture2D>("statuses/regeneration"), Regenerate, Colors.LightBlue);
            Bleeding = new StatusType("Bleeding", content.Load<Texture2D>("statuses/bleed"), Bleed, Colors.Red);
            Poisoning = new StatusType("Poisoned", content.Load<Texture2D>("statuses/poison"), Poison, Colors.LightGreen);
        }

        public StatusType Regeneration { get; private set; }
        public void Regenerate(IFighter fighter)
        {
            fighter.Health.Current++;
        }

        public StatusType Bleeding { get; private set; }
        public void Bleed(IFighter fighter)
        {
            if (fighter.Health.Current > 2)
            {
                fighter.Health.Current -= 2;
            }
        }

        public StatusType Poisoning { get; private set; }
        public void Poison(IFighter fighter)
        {
            if (fighter.Health.Current > 1)
            {
                fighter.Health.Current -= 1;
            }
        }
    }
}
