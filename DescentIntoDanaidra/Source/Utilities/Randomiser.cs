﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescentIntoDanaidra
{
    public class Randomiser
    {
        private static Random random = new Random();

        public static int RandomNumber(int min, int max)
        {
            return random.Next(min, max + 1);
        }

        public static bool PassPercentileRoll(int testedValue)
        {
            return RandomNumber(1, 100) < testedValue;
        }
    }
}
