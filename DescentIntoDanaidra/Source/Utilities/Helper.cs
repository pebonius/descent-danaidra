﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public static class Helper
    {
        public static Point CenterOnScreen(Point size)
        {
            return new Point((Game1.BufferWidth - size.X) / 2, (Game1.BufferHeight - size.Y) / 2);
        }

        public static Point ScreenSize()
        {
            return new Point(Game1.BufferWidth, Game1.BufferHeight);
        }

        public static int CenterHorizontally(int width, Rectangle rectangle)
        {
            return rectangle.Center.X - width / 2;
        }

        public static int CenterVertically(int height, Rectangle rectangle)
        {
            return rectangle.Center.Y - height / 2;
        }

        public enum Direction
        {
            None,
            Up,
            Down,
            Left,
            Right
        }

        public static Direction RandomDirection()
        {
            switch (Randomiser.RandomNumber(1, 4))
            {
                case 1: return Direction.Up;
                case 2: return Direction.Down;
                case 3: return Direction.Left;
                case 4: return Direction.Right;
                default: return Direction.None;
            }
        }

        public static Direction OppositeDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up: return Direction.Down;
                case Direction.Down: return Direction.Up;
                case Direction.Left: return Direction.Right;
                case Direction.Right:  return Direction.Left;
                default: return Direction.None;
            }
        }

        public static Vector2 NormalizedDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up: return new Vector2(0, -1);
                case Direction.Down: return new Vector2(0, 1);
                case Direction.Left: return new Vector2(-1, 0);
                case Direction.Right: return new Vector2(1, 0);
                default: return Vector2.Zero;
            }
        }

        public static string CapitalizeFirstChar(string value)
        {
            if (value != "" && value.Length > 1)
            {
                return value.First().ToString().ToUpper() + value.Substring(1).ToLower();
            }
            else return value;
        }

        public static Point MapPositionToScreen(Point position, Camera camera)
        {
            return new Point(position.X * Game1.TileSize - camera.Position.X,
                    position.Y * Game1.TileSize - camera.Position.Y);
        }

        public static Point CenterWithinTile(Point position, Point size)
        {
            return new Point(position.X + Game1.TileSize / 2 - size.X / 2, position.Y + Game1.TileSize / 2 - size.Y / 2);
        }

        public static int SizeInPixels(int value)
        {
            return value * Game1.TileSize;
        }

        public static int SizeInTiles(int value)
        {
            return value / Game1.TileSize;
        }

        public static string TruncateString(string str, int maxLength)
        {
            return str.Length <= maxLength ? str : str.Substring(0, maxLength) + "...";
        }
    }
}
