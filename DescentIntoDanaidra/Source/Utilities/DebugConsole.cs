﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class DebugConsole
    {
        private UIStateHandler ui;
        private Player player;
        private ObjectParser objectParser;

        public DebugConsole(UIStateHandler ui, Player player, ObjectParser objectParser)
        {
            this.ui = ui;
            this.player = player;
            this.objectParser = objectParser;
        }

        public void OpenDebugConsole()
        {
            ui.OpenInputPrompt("Debug Console", ParseDebugCommand, 50, true, "");
        }

        void ParseDebugCommand(string value)
        {
            if (value.StartsWith("add "))
            {
                string toParse = value.Substring(value.IndexOf(" ") + 1);
                objectParser.ParseObject(toParse, player.Location);
            }
            else if(value == "pos")
            {
                HUD.MessageLog.DisplayMessage(player.Position.ToString());
            }
            else if (value == "lvlup")
            {
                player.LevelUp();
            }
            else if (value != "")
            {
                HUD.MessageLog.DisplayMessage(value + " is not a valid command.");
            }
        }
    }
}
