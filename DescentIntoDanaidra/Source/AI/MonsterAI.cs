﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public static class MonsterAI
    {
        public static void Behave(Monster monster, Player player)
        {
            if (!monster.IsDead && !player.IsDead && monster.Position != null && player.Position != null)
            {
                TestIfSpooked(monster);

                if (SeesPlayer(monster, player))
                {
                    if (monster.IsSpooked)
                    {
                        RunAway(monster, player);
                    }
                    else
                    {
                        AttackPlayer(monster, player);
                    }
                }
                else
                {
                    WanderAround(monster);
                }
            }
        }

        static void TestIfSpooked(Monster monster)
        {
            if (AI.HasLowHealth(monster) && !monster.IsSpooked && Randomiser.PassPercentileRoll(20))
            {
                monster.IsSpooked = true;
                HUD.MessageLog.DisplayMessage(monster.Name + " cowers and runs away.");
            }
            else if (!AI.HasLowHealth(monster) && monster.IsSpooked && Randomiser.PassPercentileRoll(80))
            {
                monster.IsSpooked = false;
            }
        }

        static void WanderAround(Monster monster)
        {
            if (monster.Spawn != null && Vector2.Distance(monster.Position.GetValueOrDefault().ToVector2(),
                monster.Spawn.Position.GetValueOrDefault().ToVector2()) >= monster.WalkAround)
            {
                AI.MoveTowards(monster, monster.Spawn);
            }
            else
            {
                AI.MoveAtRandom(monster);
            }
        }

        static void AttackPlayer(Monster monster, IFighter player)
        {
            if (AI.AreNeighboring(monster, player))
            {
                Combat.MeleeHit(monster, player, monster.Type.AttackEffect);
            }
            else
            {
                AI.MoveTowards(monster, player);
            }
        }

        static void RunAway(Monster monster, IFighter player)
        {
            if (AI.CanMove(monster))
            {
                AI.MoveAwayFrom(monster, player);
            }
            else if (AI.AreNeighboring(monster, player))
            {
                Combat.MeleeHit(monster, player, monster.Type.AttackEffect);
            }
            else
            {
                // stand still
            }
        }

        static bool SeesPlayer(Monster monster, IFighter player)
        {
            return Vector2.Distance(monster.Position.GetValueOrDefault().ToVector2(), player.Position.GetValueOrDefault().ToVector2())
                <= monster.Type.DetectRange;
        }
    }
}
