﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class CharacterAI
    {
        private const int ChanceToMove = 25;

        public virtual void Behave(NPC npc, Player player)
        {
            if (TooFarFromAnchor(npc))
            {
                ReturnToAnchor(npc);
            }
            else
            {
                ResetTurnsAway(npc);
                WanderAround(npc);
            }
        }

        public virtual bool OnCollision(NPC npc, Player player)
        {
            if (npc.HasDialogueLines() || npc.SellsItems())
            {
                npc.ui.OpenDialogueMenu(npc, player, npc.timeOfDay);
            }
            if (npc.HasOpeningLines())
            {
                npc.ui.OpenDialogueLines(npc.Type.OpeningLines, npc.Name);
            }
            return false;
        }

        protected void WanderAround(NPC npc)
        {
            if (CanWalkAround(npc) && Randomiser.PassPercentileRoll(ChanceToMove))
            {
                AI.MoveAtRandom(npc);
            }
        }

        protected bool TooFarFromAnchor(NPC npc)
        {
            return Vector2.Distance(npc.Position.GetValueOrDefault().ToVector2(), npc.Anchor.ToVector2()) > npc.WalkAround;
        }

        protected void ReturnToAnchor(NPC npc)
        {
            if (npc.Position.GetValueOrDefault() != npc.Anchor)
            {
                if (npc.TurnsAwayFromAnchor > 25 && npc.CanMoveToPosition(npc.Anchor) && npc.CollidingEntity(npc.Anchor) == null)
                {
                    npc.Move(npc.Anchor);
                }
                else
                {
                    npc.TurnsAwayFromAnchor++;
                    AI.MoveTowards(npc, npc.Anchor);
                }
            }
        }

        protected void ResetTurnsAway(NPC npc)
        {
            if (npc.TurnsAwayFromAnchor != 0)
            {
                npc.TurnsAwayFromAnchor = 0;
            }
        }

        protected bool CanWalkAround(NPC npc)
        {
            return npc.Position != null && npc.WalkAround > 0;
        }
    }
}
