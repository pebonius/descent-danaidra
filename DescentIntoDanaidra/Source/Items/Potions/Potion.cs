﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;
using System.Xml;

namespace DescentIntoDanaidra
{
    public class Potion : Item, IUsable, ISaveable
    {
        public Potion(Location location, Point? position, PotionType type)
            : base(location, position)
        {
            Type = type;
            IsConsumable = true;
        }

        public Potion(PotionType type)
            : this(null, null, type)
        {
        }

        public PotionType Type { get; private set; }
        public override string Name { get { return Type.Name; } }
        public override string TypeName { get { return "Potion"; } }
        public override Texture2D Texture { get { return Type.Texture; } }
        public override string Description { get { return Type.Description; } }
        public override string PropertyLine1 { get { return Type.PropertyLine1; } }
        public override int Weight { get { return Type.Weight; } }
        public override int Value { get { return Type.Value; } }
        public bool IsConsumable { get; private set; }

        public bool Use(Player player)
        {
            HUD.MessageLog.DisplayMessage("You drink " + Name + ".");
            FloatingTextHandler.DisplayMapText(new FloatingText("Gulp!", Colors.FontWhite, player.Position.GetValueOrDefault()));
            SoundHandler.PlaySound(SoundLibrary.DrinkGulp);
            Type.OnDrink(player, Type.Restore);
            return true;
        }

        public override Item Clone(Location location, Point? position)
        {
            return new Potion(location, position, Type);
        }

        public void Save(XmlWriter writer)
        {
            writer.WriteStartElement(GetType().Name);
            writer.WriteAttributeString("Name", Type.Name);
            writer.WriteEndElement();
        }

        public new static Potion Load(XmlReader reader, ObjectParser parser)
        {
            var type = parser.PotionTypeList.GetFromDictionary(reader["Name"]);

            if (type != null)
            {
                return new Potion(type);
            }
            return null;
        }
    }
}
