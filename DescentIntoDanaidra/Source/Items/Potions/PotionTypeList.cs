﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class PotionTypeList
    {
        private readonly StatusTypeList statusTypes;

        public PotionTypeList(ContentManager content, StatusTypeList statusTypes)
        {
            this.statusTypes = statusTypes;

            HealthVial = new PotionType("Health vial", content.Load<Texture2D>("items/healthVial"), 10, HealthDrink)
            {
                Description = "Restores a bit of the drinker's health.",
                PropertyLine1 = "+10 Health",
                Weight = 2,
                Value = 30,
                DropChance = 10,
            };

            HealthBottle = new PotionType("Health bottle", content.Load<Texture2D>("items/healthBottle"), 25, HealthDrink)
            {
                Description = "Restores some of the drinker's health.",
                PropertyLine1 = "+25 Health",
                Weight = 3,
                Value = 75,
                DropChance = 7,
            };

            ManaVial = new PotionType("Mana vial", content.Load<Texture2D>("items/manaVial"), 10, ManaDrink)
            {
                Description = "Restores a bit of the drinker's mana.",
                PropertyLine1 = "+10 Mana",
                Weight = 2,
                Value = 40,
                DropChance = 10,
            };

            ManaBottle = new PotionType("Mana bottle", content.Load<Texture2D>("items/manaBottle"), 25, ManaDrink)
            {
                Description = "Restores a bit of the drinker's mana.",
                PropertyLine1 = "+25 Mana",
                Weight = 3,
                Value = 75,
                DropChance = 7,
            };

            Antidote = new PotionType("Antidote", content.Load<Texture2D>("items/antidote"), 0, AntidoteDrink)
            {
                Description = "Negates the effects of any poison, but tastes like bad conscience.",
                PropertyLine1 = "Remove poisoning",
                Weight = 2,
                Value = 35,
                DropChance = 10,
            };

            CongealBlood = new PotionType("Congeal blood", content.Load<Texture2D>("items/congeal"), 0, CongealDrink)
            {
                Description = "Stops hemorrhage by thickening the drinker's blood.",
                PropertyLine1 = "Remove bleeding",
                Weight = 2,
                Value = 45,
                DropChance = 10,
            };

            Dictionary = new Dictionary<string, PotionType>()
            {
                { HealthVial.Name.ToLower(), HealthVial },
                { HealthBottle.Name.ToLower(), HealthBottle },
                { ManaVial.Name.ToLower(), ManaVial },
                { ManaBottle.Name.ToLower(), ManaBottle },
                { Antidote.Name.ToLower(), Antidote },
                { CongealBlood.Name.ToLower(), CongealBlood },
            };
        }

        public Dictionary<string, PotionType> Dictionary { get; private set; }

        public PotionType GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, PotionType> pair in Dictionary)
            {
                if (Dictionary.TryGetValue(key.ToLower(), out PotionType type))
                {
                    return type;
                }
            }
            return null;
        }

        public PotionType GetRandom()
        {
            if (Dictionary != null && Dictionary.Any())
            {
                return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
            }
            return null;
        }

        public PotionType HealthVial { get; }
        public PotionType HealthBottle { get; }
        private void HealthDrink(Player drinker, int restore)
        {
            drinker.Health.Current += restore;
            HUD.MessageLog.DisplayMessage("You regain " + restore + " health.", Colors.LightGreen);
        }

        public PotionType ManaVial { get; }
        public PotionType ManaBottle { get; }
        private void ManaDrink(Player drinker, int restore)
        {
            drinker.Mana.Current += restore;
            HUD.MessageLog.DisplayMessage("You regain " + restore + " mana.", Colors.LightGreen);
        }

        public PotionType Antidote { get; }
        private void AntidoteDrink(Player drinker, int restore)
        {
            drinker.RemoveStatus(statusTypes.Poisoning);
            HUD.MessageLog.DisplayMessage("You are no longer poisoned.", Colors.LightGreen);
        }

        public PotionType CongealBlood { get; }
        private void CongealDrink(Player drinker, int restore)
        {
            drinker.RemoveStatus(statusTypes.Bleeding);
            HUD.MessageLog.DisplayMessage("You are no longer bleeding.", Colors.LightGreen);
        }
    }
}
