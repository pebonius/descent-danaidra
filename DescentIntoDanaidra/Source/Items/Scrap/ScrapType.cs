﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DescentIntoDanaidra
{
    public class ScrapType : ILootableType
    {
        public ScrapType(string name, Texture2D texture)
        {
            Name = name;
            Texture = texture;
        }

        public string Name { get; private set; }
        public Texture2D Texture { get; private set; }
        public string Description { get; set; }
        public int Weight { get; set; }
        public int Value { get; set; }
        public int DropChance { get; set; }
    }
}
