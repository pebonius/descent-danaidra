﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;
using System.Xml;

namespace DescentIntoDanaidra
{
    public class Scrap : Item, ISaveable
    {
        public Scrap(Location location, Point? position, ScrapType type)
            : base(location, position)
        {
            Type = type;
            IsConsumable = false;
        }

        public Scrap(ScrapType type)
            : this(null, null, type)
        {
        }

        public ScrapType Type { get; private set; }
        public override string Name { get { return Type.Name; } }
        public override string TypeName { get { return "Scrap"; } }
        public override string Description { get { return Type.Description; } }
        public override Texture2D Texture { get { return Type.Texture; } }
        public bool IsConsumable { get; private set; }
        public override int Value { get { return Type.Value; } }
        public override int Weight { get { return Type.Weight; } }

        public override Item Clone(Location location, Point? position)
        {
            return new Scrap(location, position, Type);
        }

        public void Save(XmlWriter writer)
        {
            writer.WriteStartElement(GetType().Name);
            writer.WriteAttributeString("Name", Type.Name);
            writer.WriteEndElement();
        }

        public new static Scrap Load(XmlReader reader, ObjectParser parser)
        {
            var type = parser.ScrapTypeList.GetFromDictionary(reader["Name"]);

            if (type != null)
            {
                return new Scrap(type);
            }
            return null;
        }
    }
}
