﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Xml;
using System.Xml.Serialization;

namespace DescentIntoDanaidra
{
    public class Item : IMapObject, IInventorable
    {
        public const int VanishAfterTurns = TimeOfDay.TurnsPerCycle;
        private int turnsSinceDropped;

        public Item(Location location, Point? position)
        {
            if (location != null && position != null)
            {
                location.Tilemap.ChangeItemLocation(this, location, position);
            }
            Name = "Item";
            Description = "Nothing interesting to say about this item.";
            turnsSinceDropped = 0;
        }
        
        public Item()
            : this(null, null)
        {
        }

        public virtual string Name { get; set; }
        public virtual string TypeName { get { return "Item"; } }
        public virtual string Description { get; set; }
        public virtual string PropertyLine1 { get { return ""; } }
        public virtual string PropertyLine2 { get { return ""; } }
        public virtual string PropertyLine3 { get { return ""; } }
        public virtual Texture2D Texture { get; set; }
        public bool IsDead { get; set; }
        public virtual int Value { get; set; }
        public virtual int Weight { get; set; }
        public Location Location { get; set; }
        public Point? Position { get; set; }

        public bool OnCollision(Player player)
        {
            return false;
        }

        public void Update(Player player)
        {
            if (!(Location is DungeonLevel))
            {
                CheckVanish();
            }
        }

        private void CheckVanish()
        {
            if (turnsSinceDropped <= VanishAfterTurns)
            {
                turnsSinceDropped++;
            }
            else
            {
                IsDead = true;
            }
        }

        public void Drop(Location location, Point position)
        {
            if (location != null && position != null)
            {
                location.Tilemap.ChangeItemLocation(this, location, position);
            }
            turnsSinceDropped = 0;
        }

        public virtual Item Clone(Location location, Point? position)
        {
            return new Item(location, position)
            {
                Name = this.Name,
                Texture = this.Texture,
            };
        }

        public virtual Item Clone()
        {
            return Clone(null, null);
        }

        public static Item Load(XmlReader reader, ObjectParser parser)
        {
            //TODO: remove
            switch (reader.Name)
            {
                case "Food":
                    return Food.Load(reader, parser);
                case "Book":
                    return Book.Load(reader, parser);
                case "Potion":
                    return Potion.Load(reader, parser);
                case "Scrap":
                    return Scrap.Load(reader, parser);
                case "Armor":
                    return Armor.Load(reader, parser);
                case "Weapon":
                    return Weapon.Load(reader, parser);
                default:
                    return null;
            }
        }
    }
}
