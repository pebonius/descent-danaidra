﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class FoodTypeList
    {
        public FoodTypeList(ContentManager content, StatusTypeList statusTypes)
        {
            Mushroom = new FoodType("Mushroom", content.Load<Texture2D>("items/mushroom"))
            {
                Description = "It's a mushroom.",
                FillFood = 10,
                Value = 10,
                Weight = 1,
                DropChance = 15,
            };

            Meat = new FoodType("Meat", content.Load<Texture2D>("items/meat"))
            {
                Description = "It's meat.",
                FillFood = 35,
                Value = 24,
                Weight = 3,
                DropChance = 20,
            };

            Herbs = new FoodType("Herbs", content.Load<Texture2D>("items/herbs"))
            {
                Description = "It's a bunch of herbs.",
                FillFood = 5,
                EatingEffect = statusTypes.Regeneration,
                EffectLasts = 25,
                Value = 24,
                Weight = 1,
                DropChance = 30,
            };

            Cabbage = new FoodType("Cabbage", content.Load<Texture2D>("items/cabbage"))
            {
                Description = "It's a cabbage.",
                FillFood = 15,
                Value = 24,
                Weight = 2,
                DropChance = 20,
            };

            Peas = new FoodType("Peas", content.Load<Texture2D>("items/peas"))
            {
                Description = "It's peas.",
                FillFood = 15,
                Value = 24,
                Weight = 1,
                DropChance = 30,
            };

            Monsterfruit = new FoodType("Monsterfruit", content.Load<Texture2D>("items/monsterfruit"))
            {
                Description = "This slimy and pungent fruit may cause unwanted side effects when consumed.",
                FillFood = 8,
                EatingEffect = statusTypes.Poisoning,
                EffectLasts = 10,
                Value = 3,
                Weight = 1,
                DropChance = 15,
            };

            Cheese = new FoodType("Cheese", content.Load<Texture2D>("items/cheese"))
            {
                Description = "It's cheese.",
                FillFood = 20,
                Value = 10,
                Weight = 2,
                DropChance = 20,
            };

            Turnip = new FoodType("Turnip", content.Load<Texture2D>("items/turnip"))
            {
                Description = "It's a turnip.",
                FillFood = 15,
                Value = 24,
                Weight = 2,
                DropChance = 30,
            };

            Fish = new FoodType("Fish", content.Load<Texture2D>("items/fish"))
            {
                Description = "It's a fish.",
                FillFood = 25,
                Value = 20,
                Weight = 3,
                DropChance = 20,
            };

            Bread = new FoodType("Bread", content.Load<Texture2D>("items/bread"))
            {
                Description = "It's bread.",
                FillFood = 20,
                Value = 20,
                Weight = 3,
                DropChance = 20,
            };

            Dictionary = new Dictionary<string, FoodType>()
            {
                { Mushroom.Name.ToLower(), Mushroom },
                { Meat.Name.ToLower(), Meat },
                { Herbs.Name.ToLower(), Herbs },
                { Cabbage.Name.ToLower(), Cabbage },
                { Peas.Name.ToLower(), Peas },
                { Monsterfruit.Name.ToLower(), Monsterfruit },
                { Turnip.Name.ToLower(), Turnip },
                { Cheese.Name.ToLower(), Cheese },
                { Fish.Name.ToLower(), Fish },
                { Bread.Name.ToLower(), Bread },
            };
        }

        public Dictionary<string, FoodType> Dictionary { get; private set; }
        
        public FoodType GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, FoodType> pair in Dictionary)
            {
                if (Dictionary.TryGetValue(key.ToLower(), out FoodType type))
                {
                    return type;
                }
            }
            return null;
        }

        public FoodType GetRandom()
        {
            if (Dictionary != null && Dictionary.Any())
            {
                return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
            }
            return null;
        }

        public FoodType Mushroom { get; private set; }
        public FoodType Meat { get; private set; }
        public FoodType Herbs { get; private set; }
        public FoodType Cabbage { get; private set; }
        public FoodType Peas { get; private set; }
        public FoodType Monsterfruit { get; private set; }
        public FoodType Turnip { get; private set; }
        public FoodType Cheese { get; private set; }
        public FoodType Fish { get; private set; }
        public FoodType Bread { get; private set; }
    }
}
