﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;
using System.Xml;

namespace DescentIntoDanaidra
{
    public class Food : Item, IUsable, ISaveable
    {
        public Food(Location location, Point? position, FoodType type)
            : base(location, position)
        {
            Type = type;
            IsConsumable = true;
        }

        public Food(FoodType type)
            : this(null, null, type)
        {
        }

        public FoodType Type { get; private set; }
        public override string Name { get { return Type.Name; } }
        public override string TypeName { get { return "Food"; } }
        public override string Description { get { return Type.Description; } }
        public override string PropertyLine1 { get { return "+" + Type.FillFood + " Food"; } }
        public override Texture2D Texture { get { return Type.Texture; } }
        public bool IsConsumable { get; private set; }
        public override int Value { get { return Type.Value; } }
        public override int Weight { get { return Type.Weight; } }

        public bool Use(Player player)
        {
            if (player.Food.Current < player.Food.Max)
            {
                player.Food.Current += Type.FillFood;
                SoundHandler.PlaySound(SoundLibrary.FoodMunch);
                FloatingTextHandler.DisplayMapText(new FloatingText("Yum!", Colors.FontWhite, player.Position.GetValueOrDefault()));
                HUD.MessageLog.DisplayMessage("You eat the " + Name + ".");
                if (Type.EatingEffect != null)
                {
                    player.AddStatus(new Status(Type.EatingEffect, Type.EffectLasts));
                    HUD.MessageLog.DisplayMessage("You are " + Type.EatingEffect.Name + ".", Type.EatingEffect.Color);
                }
                return true;
            }
            HUD.MessageLog.DisplayMessage("You are too full to eat this.", Colors.Orange);
            return false;
        }

        public override Item Clone(Location location, Point? position)
        {
            return new Food(location, position, Type);
        }

        public void Save(XmlWriter writer)
        {
            writer.WriteStartElement(GetType().Name);
            writer.WriteAttributeString("Name", Type.Name);
            writer.WriteEndElement();
        }

        public new static Food Load(XmlReader reader, ObjectParser parser)
        {
            var type = parser.FoodTypeList.GetFromDictionary(reader["Name"]);

            if (type != null)
            {
                return new Food(type);
            }
            return null;
        }
    }
}
