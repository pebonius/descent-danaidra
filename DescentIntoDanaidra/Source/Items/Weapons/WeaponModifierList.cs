﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class WeaponModifierList
    {
        private readonly StatusTypeList statusTypes;

        public WeaponModifierList(StatusTypeList statusTypes)
        {
            this.statusTypes = statusTypes;

            Stinging = new WeaponModifier("stinging")
            {
                Description = "Inflicts minor poison damage.",
                AttackEffect = StingingEffect,
                AddedValue = 0.25f,
            };

            Bloodthirsty = new WeaponModifier("bloodthirsty")
            {
                Description = "Drains life from the target.",
                AttackEffect = BloodthirstyEffect,
                AddedValue = 0.25f,
            };

            Sacrificial = new WeaponModifier("sacrificial")
            {
                Description = "Sacrifices some of the attacker's health to inflict extra damage.",
                AttackEffect = OfSacrificeEffect,
                AddedValue = 0.25f,
            };

            Bleeding = new WeaponModifier("bleeding")
            {
                Description = "10% chance of opening a bleeding wound.",
                AttackEffect = OfBleedingEffect,
                AddedValue = 0.25f,
            };

            Piercing = new WeaponModifier("piercing")
            {
                Description = "15% chance of inflicting piercing damage.",
                AttackEffect = OfPiercingEffect,
                AddedValue = 0.25f,
            };

            Dictionary = new Dictionary<string, WeaponModifier>()
            {
                { Stinging.Name.ToLower(), Stinging },
                { Bloodthirsty.Name.ToLower(), Bloodthirsty },
                { Sacrificial.Name.ToLower(), Sacrificial },
                { Bleeding.Name.ToLower(), Bleeding },
                { Piercing.Name.ToLower(), Piercing },
            };
        }

        public Dictionary<string, WeaponModifier> Dictionary { get; private set; }

        public WeaponModifier GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, WeaponModifier> pair in Dictionary)
            {
                if (key != null && Dictionary.TryGetValue(key.ToLower(), out WeaponModifier type))
                {
                    return type;
                }
            }
            return null;
        }

        public WeaponModifier GetRandom()
        {
            if (Dictionary != null && Dictionary.Any())
            {
                return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
            }
            return null;
        }

        public WeaponModifier Stinging { get; private set; }
        void StingingEffect(IFighter attacker, IFighter target)
        {
            target.AddStatus(new Status(statusTypes.Poisoning, Randomiser.RandomNumber(3, 5)));
            HUD.MessageLog.DisplayMessage(attacker.Name + "'s " + attacker.EquippedWeapon.Name + " poisons " +
                target.Name + ".", Colors.SteelGray);
        }

        public WeaponModifier Bloodthirsty { get; private set; }
        void BloodthirstyEffect(IFighter attacker, IFighter target)
        {
            int drain = 1;
            target.Health.Current -= drain;
            attacker.Health.Current += drain;
            HUD.MessageLog.DisplayMessage(attacker.Name + "'s " + attacker.EquippedWeapon.Name + " drains " +
                drain + " life from " + target.Name + ".", Colors.SteelGray);
        }

        public WeaponModifier Sacrificial { get; private set; }
        void OfSacrificeEffect(IFighter attacker, IFighter target)
        {
            int sacrifice = Randomiser.RandomNumber(1, 3);
            if (attacker.Health.Current > sacrifice)
            {
                attacker.Health.Current -= sacrifice;
                target.Health.Current -= sacrifice * 2;
                HUD.MessageLog.DisplayMessage(attacker.Name + "'s " + attacker.EquippedWeapon.Name + " drains " +
                    sacrifice + " life from " + attacker.Name + " to inflict more damage.", Colors.SteelGray);
            }
        }

        public WeaponModifier Bleeding { get; private set; }
        void OfBleedingEffect(IFighter attacker, IFighter target)
        {
            if (Randomiser.PassPercentileRoll(10))
            {
                target.AddStatus(new Status(statusTypes.Bleeding, Randomiser.RandomNumber(3, 7)));
                HUD.MessageLog.DisplayMessage(attacker.Name + "'s " + attacker.EquippedWeapon.Name +
                    " inflicts a bleeding wound on " + target.Name + ".", Colors.SteelGray);
            }
        }

        public WeaponModifier Piercing { get; private set; }
        void OfPiercingEffect(IFighter attacker, IFighter target)
        {
            int damage = Randomiser.RandomNumber(3, 5);
            if (Randomiser.PassPercentileRoll(15))
            {
                target.Health.Current -= damage;
                HUD.MessageLog.DisplayMessage(attacker.Name + "'s " + attacker.EquippedWeapon.Name + " deals " +
                    damage + " piercing damage to " + target.Name + ".", Colors.SteelGray);
            }
        }
    }
}
