﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescentIntoDanaidra
{
    public class WeaponModifier
    {
        public WeaponModifier(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
        public Action<IFighter, IFighter> AttackEffect { get; set; }
        public string Description { get; set; }
        public float AddedValue { get; set; }
    }
}
