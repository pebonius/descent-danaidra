﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class ArmorTypeList
    {
        public ArmorTypeList(ContentManager content)
        {
            Robe = new ArmorType("Robe", content.Load<Texture2D>("items/robe"))
            {
                Description = "A plain-looking robe.",
                Value = 50,
                Weight = 5,
                Defense = 1,
                Durability = 10,
                DropChance = 10,
            };

            Brigandine = new ArmorType("Brigandine", content.Load<Texture2D>("items/brigandine"))
            {
                Description = "A thick, hardened tunic.",
                Value = 120,
                Weight = 7,
                Defense = 3,
                Durability = 14,
                DropChance = 5,
            };

            Scalemail = new ArmorType("Scalemail", content.Load<Texture2D>("items/scalemail"))
            {
                Description = "Made of tiny metal scales.",
                Value = 250,
                Weight = 8,
                Defense = 4,
                Durability = 16,
                DropChance = 3,
            };

            Platemail = new ArmorType("Platemail", content.Load<Texture2D>("items/platemail"))
            {
                Description = "Heavy armor made of hammered steel.",
                Value = 735,
                Weight = 12,
                Defense = 8,
                Durability = 20,
                DropChance = 1,
            };

            Dictionary = new Dictionary<string, ArmorType>()
            {
                { Robe.Name.ToLower(), Robe },
                { Brigandine.Name.ToLower(), Brigandine },
                { Scalemail.Name.ToLower(), Scalemail },
                { Platemail.Name.ToLower(), Platemail },
            };
        }

        public Dictionary<string, ArmorType> Dictionary { get; private set; }

        public ArmorType GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, ArmorType> pair in Dictionary)
            {
                if (Dictionary.TryGetValue(key.ToLower(), out ArmorType type))
                {
                    return type;
                }
            }
            return null;
        }

        public ArmorType GetRandom()
        {
            if (Dictionary != null && Dictionary.Any())
            {
                return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
            }
            return null;
        }

        public ArmorType Robe { get; }
        public ArmorType Brigandine { get; }
        public ArmorType Platemail { get; }
        public ArmorType Scalemail { get; }
    }
}
