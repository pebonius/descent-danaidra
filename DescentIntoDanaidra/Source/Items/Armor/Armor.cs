﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;
using System.Xml;

namespace DescentIntoDanaidra
{
    public class Armor : Item, ISaveable
    {
        public Armor(Location location, Point? position, ArmorType type, ArmorModifier modifier)
            : base(location, position)
        {
            Type = type;
            Durability = new Stat("Durability", type.Durability);
            Defense = type.Defense;

            Modifier = modifier;
            Name = CreateName();
            Description = CreateDescription();
        }

        public Armor(ArmorType type, ArmorModifier modifier)
            : this(null, null, type, modifier)
        {
        }

        public ArmorType Type { get; private set; }
        public ArmorModifier Modifier { get; }
        public override string Name { get; set; }
        public override string TypeName { get { return "Armor"; } }
        public override string Description { get; set; }
        public override string PropertyLine1 { get { return "Defense: " + Defense; } }
        public override string PropertyLine2 { get { return "Durab.: " + Durability.Current + "/" + Durability.Max; } }
        public override Texture2D Texture { get { return Type.Texture; } }
        public override int Weight { get { return Type.Weight; } }
        public override int Value { get { return CalculateValue(); } }
        public Stat Durability { get; private set; }
        public int Defense { get; private set; }

        public void InflictEffects(IFighter wearer, IFighter attacker)
        {
            if (Modifier != null)
            {
                Modifier.Effect?.Invoke(wearer, attacker);
            }
        }

        public override Item Clone(Location location, Point? position)
        {
            return new Armor(location, position, Type, Modifier);
        }

        public string CreateName()
        {
            string name = Type.Name;

            if (Modifier != null && Modifier.Name != "")
            {
                name = Modifier.Name + " " + name;
            }
            return Helper.CapitalizeFirstChar(name);
        }

        public string CreateDescription()
        {
            string description = Type.Description;

            if (Modifier != null)
            {
                if (Modifier.Description != "")
                {
                    description += " br " + Modifier.Description;
                }
            }
            description += " br ";
            description += " br " + "Defense: " + Defense;
            description += " br " + "Durab.: " + Durability.Current + "/" + Durability.Max;

            return description;
        }

        public int CalculateValue()
        {
            int value;

            value = Type.Value;

            if (Modifier != null)
            {
                value = (int)(value * (1 + Modifier.AddedValue));
            }
            return value * Durability.Current / Durability.Max;
        }

        public void Save(XmlWriter writer)
        {
            writer.WriteStartElement(GetType().Name);
            writer.WriteAttributeString("Name", Type.Name);
            if (Modifier != null)
            {
                writer.WriteAttributeString("Modifier", Modifier.Name);
            }
            writer.WriteEndElement();
        }

        public new static Armor Load(XmlReader reader, ObjectParser parser)
        {
            var type = parser.ArmorTypeList.GetFromDictionary(reader["Name"]);
            var modifier = parser.ArmorModifiersList.GetFromDictionary(reader["Modifier"]);

            if (type != null)
            {
                return new Armor(type, modifier);
            }
            
            return null;
        }
    }
}
