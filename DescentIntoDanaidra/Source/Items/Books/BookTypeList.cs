﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class BookTypeList
    {
        private readonly StatusTypeList statusTypes;
        private readonly SkillTypeList skillTypes;

        public BookTypeList(ContentManager content, StatusTypeList statusTypes, SkillTypeList skillTypes)
        {
            this.statusTypes = statusTypes;
            this.skillTypes = skillTypes;

            FireballBook = new SkillbookType(skillTypes.CastFireball, content.Load<Texture2D>("items/book"))
            {
                Value = 500,
                DropChance = 1,
            };

            HealingBook = new SkillbookType(skillTypes.Healing, content.Load<Texture2D>("items/book"))
            {
                Value = 500,
                DropChance = 1,
            };

            Dictionary = new Dictionary<string, BookType>()
            {
                { FireballBook.Name.ToLower(), FireballBook },
                { HealingBook.Name.ToLower(), HealingBook },
            };
        }

        public Dictionary<string, BookType> Dictionary { get; private set; }

        public BookType GetFromDictionary(string key)
        {
            foreach (KeyValuePair<string, BookType> pair in Dictionary)
            {
                if (Dictionary.TryGetValue(key.ToLower(), out BookType type))
                {
                    return type;
                }
            }
            return null;
        }

        public BookType GetRandom()
        {
            if (Dictionary != null && Dictionary.Any())
            {
                return Dictionary.ElementAt(Randomiser.RandomNumber(0, Dictionary.Count - 1)).Value;
            }
            return null;
        }

        public SkillbookType FireballBook { get; }
        public SkillbookType HealingBook { get; }
    }
}
