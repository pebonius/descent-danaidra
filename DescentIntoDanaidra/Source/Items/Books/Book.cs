﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;
using System.Xml;

namespace DescentIntoDanaidra
{
    public class Book : Item, IUsable, ISaveable
    {
        public Book(Location location, Point? position, BookType type)
            : base(location, position)
        {
            Type = type;
            IsConsumable = false;
        }

        public Book(BookType type)
            : this(null, null, type)
        {
        }

        public BookType Type { get; private set; }
        public override string Name { get { return Type.Name; } }
        public override string TypeName { get { return Type.TypeName; } }
        public override Texture2D Texture { get { return Type.Texture; } }
        public override string Description { get { return Type.Description; } }
        public override int Weight { get { return Type.Weight; } }
        public override int Value { get { return Type.Value; } }
        public bool IsConsumable { get; private set; }

        public bool Use(Player player)
        {
            HUD.MessageLog.DisplayMessage("You open " + Name + ".");
            Type.Read(player);
            return true;
        }

        public override Item Clone(Location location, Point? position)
        {
            return new Book(location, position, Type);
        }

        public void Save(XmlWriter writer)
        {
            writer.WriteStartElement(GetType().Name);
            writer.WriteAttributeString("Name", Type.Name);
            writer.WriteEndElement();
        }

        public new static Book Load(XmlReader reader, ObjectParser parser)
        {
            var type = parser.BookTypeList.GetFromDictionary(reader["Name"]);

            if (type != null)
            {
                return new Book(type);
            }
            return null;
        }
    }
}
