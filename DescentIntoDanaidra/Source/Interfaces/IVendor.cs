﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescentIntoDanaidra
{
    public interface IVendor : IMapObject
    {
        string Name { get; }
        List<Item> Equipment { get; }
        int EquipmentSlots { get; }
        Stat Gold { get; }
    }
}
