﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace DescentIntoDanaidra
{
    public interface ISaveable
    {
        void Save(XmlWriter writer);
    }
}
