﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using DescentIntoDanaidra.UI;


namespace DescentIntoDanaidra
{
    public class SkillType
    {
        public SkillType(string name, Texture2D texture, int maxLevel, int manaCost, bool takesTurn, Action<Player, Skill> effect)
        {
            Name = name;
            Description = "Undefined.";
            Texture = texture;
            MaxLevel = maxLevel;
            ManaCost = manaCost;
            TakesTurn = takesTurn;
            Effect = effect;
        }

        public string Name { get; private set; }
        public string Description { get; set; }
        public Texture2D Texture { get; private set; }
        public int MaxLevel { get; private set; }
        public int ManaCost { get; private set; }
        public bool TakesTurn { get; private set; }
        public Action<Player, Skill> Effect { get; private set; }
    }
}
