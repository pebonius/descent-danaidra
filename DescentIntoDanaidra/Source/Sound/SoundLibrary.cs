﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using DescentIntoDanaidra.UI;

namespace DescentIntoDanaidra
{
    public class SoundLibrary
    {
        public SoundLibrary(ContentManager content)
        {
            CoinJingle = content.Load<SoundEffect>("sounds/coins");
            FoodMunch = content.Load<SoundEffect>("sounds/munchmunch");
            PageTurn = content.Load<SoundEffect>("sounds/pageTurn");
            PageTurnB = content.Load<SoundEffect>("sounds/pageTurnB");
            Shimmer = content.Load<SoundEffect>("sounds/shimmer1");
            DrinkGulp = content.Load<SoundEffect>("sounds/drink");
            PushRumble = content.Load<SoundEffect>("sounds/push");
            BagRustle = content.Load<SoundEffect>("sounds/rustle");
            SpookyDrone = content.Load<SoundEffect>("sounds/spookyDrone");
            HitSquelch = content.Load<SoundEffect>("sounds/combatHit2");
            ClangSquelch = content.Load<SoundEffect>("sounds/combatHit1");
            Clang = content.Load<SoundEffect>("sounds/combatHit3");
        }

        public static SoundEffect CoinJingle { get; private set; }
        public static SoundEffect FoodMunch { get; private set; }
        public static SoundEffect PageTurn { get; private set; }
        public static SoundEffect PageTurnB { get; private set; }
        public static SoundEffect Shimmer { get; private set; }
        public static SoundEffect DrinkGulp { get; private set; }
        public static SoundEffect PushRumble { get; private set; }
        public static SoundEffect BagRustle { get; private set; }
        public static SoundEffect SpookyDrone { get; private set; }
        public static SoundEffect HitSquelch { get; private set; }
        public static SoundEffect ClangSquelch { get; private set; }
        public static SoundEffect Clang { get; private set; }
    }
}
